package com.agro.quality_control.di;

import android.app.Application;

import com.agro.quality_control.App;
import com.agro.quality_control.di.modules.ActivityBindingModule;
import com.agro.quality_control.di.modules.ApplicationModule;
import com.agro.quality_control.di.modules.FirebaseModule;
import com.agro.quality_control.di.modules.FragmentBindingModule;
import com.agro.quality_control.di.modules.ResourceModule;
import com.agro.quality_control.di.modules.RoomModule;
import com.agro.quality_control.di.scopes.ApplicationScope;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@ApplicationScope
@Component(modules = {
        ApplicationModule.class,
        FirebaseModule.class,
        ResourceModule.class,
        RoomModule.class,
        ActivityBindingModule.class,
        FragmentBindingModule.class,
        AndroidSupportInjectionModule.class})
public interface AppComponent extends AndroidInjector<App> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        AppComponent.Builder application(Application application);

        AppComponent build();
    }
}
