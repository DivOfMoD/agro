package com.agro.quality_control.di.modules.activities;


import com.agro.quality_control.di.scopes.AdminScope;
import com.agro.quality_control.ui.activities.admin.AdminActivity;
import com.agro.quality_control.ui.activities.admin.AdminContract;
import com.agro.quality_control.ui.activities.admin.AdminModel;
import com.agro.quality_control.ui.activities.admin.AdminPresenter;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class AdminModule {

    @AdminScope
    @Binds
    public abstract AdminContract.AdminModel bindModel(AdminModel pAdminModel);

    @AdminScope
    @Binds
    public abstract AdminContract.AdminPresenter bindPresenter(AdminPresenter pAdminPresenter);

    @AdminScope
    @Binds
    abstract AdminContract.AdminView bindView(AdminActivity pAdminActivity);
}
