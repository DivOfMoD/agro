package com.agro.quality_control.di.modules.fragments;

import com.agro.quality_control.di.scopes.CreateReportScope;
import com.agro.quality_control.ui.fragments.createreport.CreateReportContract;
import com.agro.quality_control.ui.fragments.createreport.CreateReportFragment;
import com.agro.quality_control.ui.fragments.createreport.CreateReportModel;
import com.agro.quality_control.ui.fragments.createreport.CreateReportPresenter;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class CreateReportModule {

    @CreateReportScope
    @Binds
    public abstract CreateReportContract.CreateReportModel bindModel(CreateReportModel pCreateReportModel);

    @CreateReportScope
    @Binds
    public abstract CreateReportContract.CreateReportPresenter bindPresenter(CreateReportPresenter pCreateReportPresenter);

    @CreateReportScope
    @Binds
    abstract CreateReportContract.CreateReportView view(CreateReportFragment pCreateReportFragment);

}
