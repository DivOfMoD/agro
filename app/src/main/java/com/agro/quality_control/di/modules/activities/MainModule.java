package com.agro.quality_control.di.modules.activities;

import com.agro.quality_control.di.scopes.MainScope;
import com.agro.quality_control.ui.activities.main.MainActivity;
import com.agro.quality_control.ui.activities.main.MainContract;
import com.agro.quality_control.ui.activities.main.MainModel;
import com.agro.quality_control.ui.activities.main.MainPresenter;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class MainModule {

    @MainScope
    @Binds
    public abstract MainContract.MainModel bindModel(MainModel pMainModel);

    @MainScope
    @Binds
    public abstract MainContract.MainPresenter bindPresenter(MainPresenter pMainPresenter);

    @MainScope
    @Binds
    abstract MainContract.MainView bindView(MainActivity pMainActivity);
}
