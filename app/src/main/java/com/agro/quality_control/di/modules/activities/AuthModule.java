package com.agro.quality_control.di.modules.activities;

import com.agro.quality_control.di.scopes.AuthScope;
import com.agro.quality_control.ui.activities.auth.AuthActivity;
import com.agro.quality_control.ui.activities.auth.AuthContract;
import com.agro.quality_control.ui.activities.auth.AuthModel;
import com.agro.quality_control.ui.activities.auth.AuthPresenter;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class AuthModule {

    @AuthScope
    @Binds
    public abstract AuthContract.AuthModel bindModel(AuthModel pAuthModel);

    @AuthScope
    @Binds
    public abstract AuthContract.AuthPresenter bindPresenter(AuthPresenter pAuthPresenter);

    @AuthScope
    @Binds
    abstract AuthContract.AuthView bindView(AuthActivity pAuthActivity);
}
