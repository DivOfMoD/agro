package com.agro.quality_control.di.modules;

import com.agro.quality_control.di.modules.fragments.ArchiveModule;
import com.agro.quality_control.di.modules.fragments.CreateReportModule;
import com.agro.quality_control.di.modules.fragments.ReportsModule;
import com.agro.quality_control.di.modules.fragments.SuppliersModule;
import com.agro.quality_control.di.scopes.ArchiveScope;
import com.agro.quality_control.di.scopes.CreateReportScope;
import com.agro.quality_control.di.scopes.ReportsScope;
import com.agro.quality_control.di.scopes.SuppliersScope;
import com.agro.quality_control.ui.fragments.archive.ArchiveFragment;
import com.agro.quality_control.ui.fragments.createreport.CreateReportFragment;
import com.agro.quality_control.ui.fragments.reports.ReportsFragment;
import com.agro.quality_control.ui.fragments.suppliers.SuppliersFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentBindingModule {

    @CreateReportScope
    @ContributesAndroidInjector(modules = CreateReportModule.class)
    abstract CreateReportFragment createReportFragment();

    @ReportsScope
    @ContributesAndroidInjector(modules = ReportsModule.class)
    abstract ReportsFragment reportFragment();

    @ArchiveScope
    @ContributesAndroidInjector(modules = ArchiveModule.class)
    abstract ArchiveFragment archiveFragment();

    @SuppliersScope
    @ContributesAndroidInjector(modules = SuppliersModule.class)
    abstract SuppliersFragment suppliersFragment();

}
