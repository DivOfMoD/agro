package com.agro.quality_control.di.modules;

import android.content.Context;
import android.content.res.Resources;

import dagger.Module;
import dagger.Provides;

@Module(includes = ApplicationModule.class)
public class ResourceModule {

    @Provides
    Resources provideResources(Context pContext) {
        return pContext.getResources();
    }
}