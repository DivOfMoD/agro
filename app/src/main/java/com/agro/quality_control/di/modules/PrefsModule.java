package com.agro.quality_control.di.modules;

import android.content.Context;

import com.agro.quality_control.di.scopes.ApplicationScope;
import com.agro.quality_control.utils.Prefs;

import dagger.Module;
import dagger.Provides;

@Module(includes = com.agro.quality_control.di.modules.ApplicationModule.class)
public class PrefsModule {

    @Provides
    @ApplicationScope
    Prefs providePrefs(Context pContext) {
        return new Prefs(pContext);
    }
}