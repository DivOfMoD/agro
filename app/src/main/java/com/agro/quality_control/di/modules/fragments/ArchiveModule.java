package com.agro.quality_control.di.modules.fragments;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

import com.agro.quality_control.di.scopes.ArchiveScope;
import com.agro.quality_control.ui.adapters.ArchiveAdapter;
import com.agro.quality_control.ui.fragments.archive.ArchiveContract;
import com.agro.quality_control.ui.fragments.archive.ArchiveFragment;
import com.agro.quality_control.ui.fragments.archive.ArchiveModel;
import com.agro.quality_control.ui.fragments.archive.ArchivePresenter;
import com.agro.quality_control.ui.views.InsetDecoration;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class ArchiveModule {

    @ArchiveScope
    @Binds
    public abstract ArchiveContract.ArchiveModel bindModel(ArchiveModel pArchiveModel);

    @ArchiveScope
    @Binds
    public abstract ArchiveContract.ArchivePresenter bindPresenter(ArchivePresenter pArchivePresenter);

    @ArchiveScope
    @Binds
    abstract ArchiveContract.ArchiveView view(ArchiveFragment pArchiveFragment);

    @Provides
    static LinearLayoutManager provideLinearLayoutManager(Context pContext) {
        return new LinearLayoutManager(pContext);
    }

    @Provides
    static InsetDecoration provideInsetDecoration(Context pContext) {
        return new InsetDecoration(pContext);
    }

    @Provides
    static ArchiveAdapter provideReportsAdapter(Context pContext) {
        return new ArchiveAdapter(pContext);
    }
}
