package com.agro.quality_control.di.modules;


import android.arch.persistence.room.Room;
import android.content.Context;

import com.agro.quality_control.data.ThreadExecutors;
import com.agro.quality_control.data.db.AgroDatabase;

import dagger.Module;
import dagger.Provides;

@Module(includes = com.agro.quality_control.di.modules.ApplicationModule.class)
public class RoomModule {

    @Provides
    AgroDatabase provideRoom(Context pContext) {
        return Room.databaseBuilder(pContext, AgroDatabase.class, "AgroDatabase.db").build();
    }

    @Provides
    ThreadExecutors provideThreadExecutor() {
        return new ThreadExecutors();
    }

}