package com.agro.quality_control.di.modules.fragments;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

import com.agro.quality_control.di.scopes.SuppliersScope;
import com.agro.quality_control.ui.adapters.SuppliersAdapter;
import com.agro.quality_control.ui.fragments.suppliers.SuppliersContract;
import com.agro.quality_control.ui.fragments.suppliers.SuppliersFragment;
import com.agro.quality_control.ui.fragments.suppliers.SuppliersModel;
import com.agro.quality_control.ui.fragments.suppliers.SuppliersPresenter;
import com.agro.quality_control.ui.views.InsetDecoration;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class SuppliersModule {

    @SuppliersScope
    @Binds
    public abstract SuppliersContract.SuppliersModel bindModel(SuppliersModel pSuppliersModel);

    @SuppliersScope
    @Binds
    public abstract SuppliersContract.SuppliersPresenter bindPresenter(SuppliersPresenter pSuppliersPresenter);

    @SuppliersScope
    @Binds
    abstract SuppliersContract.SuppliersView view(SuppliersFragment pSuppliersFragment);

    @Provides
    static LinearLayoutManager provideLinearLayoutManager(Context pContext) {
        return new LinearLayoutManager(pContext);
    }

    @Provides
    static InsetDecoration provideInsetDecoration(Context pContext) {
        return new InsetDecoration(pContext);
    }

    @Provides
    static SuppliersAdapter provideSuppliersAdapter(Context pContext) {
        return new SuppliersAdapter(pContext);
    }
}
