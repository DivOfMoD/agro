package com.agro.quality_control.di.modules.fragments;


import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

import com.agro.quality_control.di.scopes.ReportsScope;
import com.agro.quality_control.ui.adapters.ReportsAdapter;
import com.agro.quality_control.ui.fragments.reports.ReportsContract;
import com.agro.quality_control.ui.fragments.reports.ReportsFragment;
import com.agro.quality_control.ui.fragments.reports.ReportsModel;
import com.agro.quality_control.ui.fragments.reports.ReportsPresenter;
import com.agro.quality_control.ui.views.InsetDecoration;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class ReportsModule {

    @ReportsScope
    @Binds
    public abstract ReportsContract.ReportsModel bindModel(ReportsModel pReportsModel);

    @ReportsScope
    @Binds
    public abstract ReportsContract.ReportsPresenter bindPresenter(ReportsPresenter pReportsPresenter);

    @ReportsScope
    @Binds
    abstract ReportsContract.ReportsView view(ReportsFragment pReportsFragment);

    @Provides
    static LinearLayoutManager provideLinearLayoutManager(Context pContext) {
        return new LinearLayoutManager(pContext);
    }

    @Provides
    static InsetDecoration provideInsetDecoration(Context pContext) {
        return new InsetDecoration(pContext);
    }

    @Provides
    static ReportsAdapter provideReportsAdapter(Context pContext) {
        return new ReportsAdapter(pContext);
    }

}
