package com.agro.quality_control.di.modules;

import com.agro.quality_control.di.modules.activities.AdminModule;
import com.agro.quality_control.di.modules.activities.AuthModule;
import com.agro.quality_control.di.modules.activities.MainModule;
import com.agro.quality_control.di.scopes.AdminScope;
import com.agro.quality_control.di.scopes.AuthScope;
import com.agro.quality_control.di.scopes.MainScope;
import com.agro.quality_control.ui.activities.admin.AdminActivity;
import com.agro.quality_control.ui.activities.auth.AuthActivity;
import com.agro.quality_control.ui.activities.main.MainActivity;
import com.agro.quality_control.ui.activities.permission.PermissionRequestActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {

    @AuthScope
    @ContributesAndroidInjector(modules = AuthModule.class)
    abstract AuthActivity authActivity();

    @MainScope
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract MainActivity mainActivity();

    @AdminScope
    @ContributesAndroidInjector(modules = AdminModule.class)
    abstract AdminActivity adminActivity();

    @ContributesAndroidInjector()
    abstract PermissionRequestActivity permissonActivity();

}
