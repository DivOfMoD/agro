package com.agro.quality_control.utils;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

public class Prefs {

    private static final String SHARED_PREFS_NAME = "_PREFS";

    private static final String KEY_PRIVACY_POLICY = "KEY_PRIVACY_POLICY";
    private static final String KEY_ADMIN = "KEY_ADMIN";

    private SharedPreferences sharedPreferences;

    @Inject
    public Prefs(Context context) {
        this.sharedPreferences = context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE);
    }

    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    public boolean isAdmin() {
        return sharedPreferences.getBoolean(KEY_ADMIN, false);
    }

    public void setAdmin(boolean pIsAdmin) {
        sharedPreferences.edit().putBoolean(KEY_ADMIN, pIsAdmin).apply();
    }

}