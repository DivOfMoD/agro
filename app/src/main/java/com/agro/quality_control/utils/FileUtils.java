package com.agro.quality_control.utils;

import android.os.Environment;

import java.io.File;
import java.io.IOException;

public abstract class FileUtils {

    public static File getLocalTempFile(String pFileName) {
        File file = null;
        try {
            File directory = getDirectoryPath();
            file = new File(directory, pFileName + ConstUtils.PDF);
            file.createNewFile();
        } catch (IOException pE) {
            pE.printStackTrace();
        }
        return file;
    }

    public static void deleteFile(String pFileName) {
        try {
            File directory = getDirectoryPath();
            File file = new File(directory, pFileName + ConstUtils.PDF);
            file.delete();
        } catch (Exception pE) {
            pE.printStackTrace();
        }
    }

    private static File getDirectoryPath() {
        String externalDirectory = Environment.getExternalStorageDirectory().toString();
        File file = new File(externalDirectory, ConstUtils.ROOT_PATH);
        if (!file.exists()) {
            file.mkdir();
        }
        return file;
    }
}
