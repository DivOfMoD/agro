package com.agro.quality_control.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

public class PermissionUtil {

    private static final String[] PERMISSIONS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE};

    public static boolean isAllNecessaryPermissionsGranted(Context context) {
        return checkAllPermissions(context, PERMISSIONS);
    }

    public static boolean checkAllPermissions(Context context, String[] permissions) {
        boolean allGranted = true;

        for (int i = 0; i < permissions.length && allGranted; i++) {
            allGranted = ContextCompat.checkSelfPermission(context, permissions[i]) == PackageManager.PERMISSION_GRANTED;
        }
        return allGranted;
    }

    public static void requestAllPermissions(Activity activity, int requestCode) {
        ActivityCompat.requestPermissions(activity, PERMISSIONS, requestCode);
    }
}
