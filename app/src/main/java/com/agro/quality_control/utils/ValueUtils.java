package com.agro.quality_control.utils;

import com.agro.quality_control.data.db.AgroDatabase;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public abstract class ValueUtils {

    private static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private static SecureRandom rnd = new SecureRandom();

    public static String randomString(int pLength) {
        StringBuilder sb = new StringBuilder(pLength);
        for (int i = 0; i < pLength; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }

    public static String getFormattedDate(long pTimestamp) {
        SimpleDateFormat formatter = new SimpleDateFormat("hh:mm - dd.MM.yyyy", Locale.getDefault());
        return formatter.format(new Date(pTimestamp));
    }

    public static String getFormattedSyncState(int pSync) {
        if (pSync == AgroDatabase.SYNC_DRAFT) {
            return "Неполный отчет";
        } else {
            return "Полный отчет";
        }
    }
}
