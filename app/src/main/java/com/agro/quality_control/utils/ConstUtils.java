package com.agro.quality_control.utils;

public interface ConstUtils {

    String FIRESTORE_COLLECTION_SUPPLIERS = "suppliers";
    String FIRESTORE_COLLECTION_REPORTS = "reports";

    String PACK_FIELD_ID = "id";
    String PACK_FIELD_NAME = "name";
    String PACK_FIELD_ADDED_DATE = "date_added";
    String PACK_FIELD_URL = "url";
    String PACK_FIELD_DATE = "date";
    String PACK_FIELD_SIZE = "size";

    String ADMIN = "admin";

    int ONE_KB = 1024;
    String PDF = ".xls";
    String ROOT_PATH = "Quality Control";
    String STORAGE_PATH = "Reports";

    String[] COLUMNS = new String[]{
            "#",
            "Время",
            "Тип отчета",
            "Номер ящика",
            "Культура",
            "Год урожая",
            "Номер накладной",
            "Масса нетто",
            "Поставщик",
            "Пункт погрузки",
            "Номер авто",
            "Влажность",
            "Клейковина, %",
            "Клейковина, ед. ИДК",
            "Натура",
            "Число падения",
            "Зараженность",
            "Засоренность",
            "Запах",
            "Пункт разгрузки (подъемник)",
            "Пункт разгрузки (цех)",
            "Пункт разгрузки (бочка)",
            "Пункт разгрузки (силос)"
    };

}
