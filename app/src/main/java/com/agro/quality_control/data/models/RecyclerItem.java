package com.agro.quality_control.data.models;

import android.support.annotation.Nullable;

public abstract class RecyclerItem {

    public static final int TYPE_HEADER = 0;
    public static final int TYPE_REPORT = 1;

    abstract public int getType();

    @Nullable
    protected boolean checked;

    @Nullable
    public boolean isChecked() {
        return checked;
    }

    public void setChecked(@Nullable boolean pChecked) {
        checked = pChecked;
    }
}
