package com.agro.quality_control.data.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.agro.quality_control.data.models.Report;

import java.util.List;

@Dao
public interface ReportDataDao {

    @Query("SELECT * FROM Report WHERE syncState = 0 OR syncState = 1 ORDER BY timestamp")
    List<Report> getAllReport();

    @Query("SELECT * FROM Report WHERE (syncState = 0 OR syncState = 1) AND docNumber LIKE :pFilter ORDER BY timestamp")
    List<Report> getFilterReport(String pFilter);

    @Query("SELECT * FROM Report WHERE syncState = 1 ORDER BY timestamp")
    List<Report> getDoneReport();

    @Insert
    void insertReportData(Report pReport);

    @Update
    void update(Report pReport);

    @Delete
    int deleteReports(List<Report> pReportList);

    @Query("SELECT COUNT(*) FROM Report WHERE syncState = 0")
    int getDraftReportCount();

    @Query("SELECT COUNT(*) FROM Report WHERE syncState = 1")
    int getDoneReportCount();
}
