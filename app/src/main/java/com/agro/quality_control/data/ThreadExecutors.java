package com.agro.quality_control.data;


import com.agro.quality_control.data.db.DBThreadCallback;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class ThreadExecutors {

    private ExecutorService dbThreadExecutor = Executors.newSingleThreadExecutor();

    public ExecutorService dbExecutor() {
        return dbThreadExecutor;
    }

    public void dbExecuteTask(Runnable runnable, DBThreadCallback callback) {
        dbThreadExecutor.execute(() -> {
            try {
                runnable.run();
                callback.onFinished();
            } catch (Exception ex) {
                callback.onFailed(ex);
            }
        });
    }
}
