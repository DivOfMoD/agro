package com.agro.quality_control.data.db;

public interface DBThreadCallback {

    void onFinished();

    void onFailed(Exception ex);

}