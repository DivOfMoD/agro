package com.agro.quality_control.data.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

@Entity
public class Report extends RecyclerItem implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private int syncState;

    @Nullable
    private String boxNumber;
    @Nullable
    private String culture;
    @Nullable
    private String gluten;
    @Nullable
    private String glutenIdk;
    @Nullable
    private String nature;
    @Nullable
    private String drop;
    @Nullable
    private String cultureYear;
    @Nullable
    private String docNumber;
    @Nullable
    private String weight;
    @Nullable
    private String supplier;
    @Nullable
    private String loading;
    @Nullable
    private String autoNumber;
    @Nullable
    private String humidity;
    @Nullable
    private boolean poison;
    @Nullable
    private String garbage;
    @Nullable
    private String smell;
    @Nullable
    private String unloading;
    @Nullable
    private String unloading2;
    @Nullable
    private long timestamp;

    public Report() {
    }

    public Report(@Nullable String pBoxNumber, @Nullable String pCulture, @Nullable String pGluten, @Nullable String pGlutenIdk, @Nullable String pNature, @Nullable String pDrop, @Nullable String pCultureYear, @Nullable String pDocNumber, @Nullable String pWeight, @Nullable String pSupplier, @Nullable String pLoading, @Nullable String pAutoNumber, @Nullable String pHumidity, @Nullable boolean pPoison, @Nullable String pGarbage, @Nullable String pSmell, @Nullable String pUnloading, @Nullable String pUnloading2) {
        boxNumber = pBoxNumber;
        culture = pCulture;
        gluten = pGluten;
        glutenIdk = pGlutenIdk;
        nature = pNature;
        drop = pDrop;
        cultureYear = pCultureYear;
        docNumber = pDocNumber;
        weight = pWeight;
        supplier = pSupplier;
        loading = pLoading;
        autoNumber = pAutoNumber;
        humidity = pHumidity;
        poison = pPoison;
        garbage = pGarbage;
        smell = pSmell;
        unloading = pUnloading;
        unloading2 = pUnloading2;
        timestamp = System.currentTimeMillis();
    }

    public Report(@Nullable String pBoxNumber, @Nullable String pDocNumber) {
        boxNumber = pBoxNumber;
        docNumber = pDocNumber;
    }

    protected Report(Parcel in) {
        id = in.readInt();
        syncState = in.readInt();
        boxNumber = in.readString();
        culture = in.readString();
        gluten = in.readString();
        glutenIdk = in.readString();
        drop = in.readString();
        culture = in.readString();
        cultureYear = in.readString();
        docNumber = in.readString();
        weight = in.readString();
        supplier = in.readString();
        loading = in.readString();
        autoNumber = in.readString();
        humidity = in.readString();
        poison = in.readByte() != 0;
        garbage = in.readString();
        smell = in.readString();
        unloading = in.readString();
        unloading2 = in.readString();
        timestamp = in.readLong();
    }

    public static final Creator<Report> CREATOR = new Creator<Report>() {
        @Override
        public Report createFromParcel(Parcel in) {
            return new Report(in);
        }

        @Override
        public Report[] newArray(int size) {
            return new Report[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int pId) {
        id = pId;
    }

    public int getSyncState() {
        return syncState;
    }

    public void setSyncState(int pSyncState) {
        syncState = pSyncState;
    }

    @Nullable
    public String getBoxNumber() {
        return boxNumber;
    }

    @Nullable
    public Integer getBoxNumberInteger() {
        return Integer.valueOf(boxNumber);
    }

    public void setBoxNumber(@Nullable String pBoxNumber) {
        boxNumber = pBoxNumber;
    }

    @Nullable
    public String getCulture() {
        return culture;
    }

    public void setCulture(@Nullable String pCulture) {
        culture = pCulture;
    }

    @Nullable
    public String getGluten() {
        return gluten;
    }

    public void setGluten(@Nullable String pGluten) {
        gluten = pGluten;
    }

    @Nullable
    public String getGlutenIdk() {
        return glutenIdk;
    }

    public void setGlutenIdk(@Nullable String pGlutenIdk) {
        glutenIdk = pGlutenIdk;
    }

    @Nullable
    public String getNature() {
        return nature;
    }

    public void setNature(@Nullable String pNature) {
        nature = pNature;
    }

    @Nullable
    public String getDrop() {
        return drop;
    }

    public void setDrop(@Nullable String pDrop) {
        drop = pDrop;
    }

    @Nullable
    public String getCultureYear() {
        return cultureYear;
    }

    public void setCultureYear(@Nullable String pCultureYear) {
        cultureYear = pCultureYear;
    }

    @Nullable
    public String getDocNumber() {
        return docNumber;
    }

    public void setDocNumber(@Nullable String pDocNumber) {
        docNumber = pDocNumber;
    }

    @Nullable
    public String getWeight() {
        return weight;
    }

    public void setWeight(@Nullable String pWeight) {
        weight = pWeight;
    }

    @Nullable
    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(@Nullable String pSupplier) {
        supplier = pSupplier;
    }

    @Nullable
    public String getLoading() {
        return loading;
    }

    public void setLoading(@Nullable String pLoading) {
        loading = pLoading;
    }

    @Nullable
    public String getAutoNumber() {
        return autoNumber;
    }

    public void setAutoNumber(@Nullable String pAutoNumber) {
        autoNumber = pAutoNumber;
    }

    @Nullable
    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(@Nullable String pHumidity) {
        humidity = pHumidity;
    }

    @Nullable
    public boolean isPoison() {
        return poison;
    }

    public void setPoison(@Nullable boolean pPoison) {
        poison = pPoison;
    }

    @Nullable
    public String getGarbage() {
        return garbage;
    }

    public void setGarbage(@Nullable String pGarbage) {
        garbage = pGarbage;
    }

    @Nullable
    public String getSmell() {
        return smell;
    }

    public void setSmell(@Nullable String pSmell) {
        smell = pSmell;
    }

    @Nullable
    public String getUnloading() {
        return unloading;
    }

    public void setUnloading(@Nullable String pUnloading) {
        unloading = pUnloading;
    }

    @Nullable
    public String getUnloading2() {
        return unloading2;
    }

    public void setUnloading2(@Nullable String pUnloading2) {
        unloading2 = pUnloading2;
    }

    @Nullable
    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(@Nullable long pTimestamp) {
        timestamp = pTimestamp;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel pParcel, int pI) {
        pParcel.writeInt(id);
        pParcel.writeInt(syncState);
        pParcel.writeString(boxNumber);
        pParcel.writeString(culture);
        pParcel.writeString(gluten);
        pParcel.writeString(glutenIdk);
        pParcel.writeString(nature);
        pParcel.writeString(drop);
        pParcel.writeString(cultureYear);
        pParcel.writeString(docNumber);
        pParcel.writeString(weight);
        pParcel.writeString(supplier);
        pParcel.writeString(loading);
        pParcel.writeString(autoNumber);
        pParcel.writeString(humidity);
        pParcel.writeByte((byte) (poison ? 1 : 0));
        pParcel.writeString(garbage);
        pParcel.writeString(smell);
        pParcel.writeString(unloading);
        pParcel.writeString(unloading2);
        pParcel.writeLong(timestamp);
    }

    @Override
    public int getType() {
        return RecyclerItem.TYPE_REPORT;
    }

    @Override
    public String toString() {
        return "Report{" +
                "id=" + id +
                ", syncState=" + syncState +
                ", boxNumber='" + boxNumber + '\'' +
                ", culture='" + culture + '\'' +
                ", gluten='" + gluten + '\'' +
                ", glutenIdk='" + glutenIdk + '\'' +
                ", nature='" + nature + '\'' +
                ", drop='" + drop + '\'' +
                ", cultureYear='" + cultureYear + '\'' +
                ", docNumber='" + docNumber + '\'' +
                ", weight='" + weight + '\'' +
                ", supplier='" + supplier + '\'' +
                ", loading='" + loading + '\'' +
                ", autoNumber='" + autoNumber + '\'' +
                ", humidity='" + humidity + '\'' +
                ", poison=" + poison +
                ", garbage='" + garbage + '\'' +
                ", smell='" + smell + '\'' +
                ", unloading='" + unloading + '\'' +
                ", unloading2='" + unloading2 + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}