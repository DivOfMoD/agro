package com.agro.quality_control.data.models;

import java.util.ArrayList;

public class LocalDropdown {
    ArrayList<String> cultureArrayList;
    ArrayList<String> cultureYearArrayList;
    ArrayList<String> garbageArrayList;
    ArrayList<String> smellArrayList;
    ArrayList<String> unloadingArrayList;

    public LocalDropdown() {
    }

    public LocalDropdown(ArrayList<String> pCultureArrayList, ArrayList<String> pCultureYearArrayList, ArrayList<String> pGarbageArrayList, ArrayList<String> pSmellArrayList, ArrayList<String> pUnloadingArrayList) {
        cultureArrayList = pCultureArrayList;
        cultureYearArrayList = pCultureYearArrayList;
        garbageArrayList = pGarbageArrayList;
        smellArrayList = pSmellArrayList;
        unloadingArrayList = pUnloadingArrayList;
    }

    public ArrayList<String> getCultureArrayList() {
        return cultureArrayList;
    }

    public void setCultureArrayList(ArrayList<String> pCultureArrayList) {
        cultureArrayList = pCultureArrayList;
    }

    public ArrayList<String> getCultureYearArrayList() {
        return cultureYearArrayList;
    }

    public void setCultureYearArrayList(ArrayList<String> pCultureYearArrayList) {
        cultureYearArrayList = pCultureYearArrayList;
    }

    public ArrayList<String> getGarbageArrayList() {
        return garbageArrayList;
    }

    public void setGarbageArrayList(ArrayList<String> pGarbageArrayList) {
        garbageArrayList = pGarbageArrayList;
    }

    public ArrayList<String> getSmellArrayList() {
        return smellArrayList;
    }

    public void setSmellArrayList(ArrayList<String> pSmellArrayList) {
        smellArrayList = pSmellArrayList;
    }

    public ArrayList<String> getUnloadingArrayList() {
        return unloadingArrayList;
    }

    public void setUnloadingArrayList(ArrayList<String> pUnloadingArrayList) {
        unloadingArrayList = pUnloadingArrayList;
    }
}
