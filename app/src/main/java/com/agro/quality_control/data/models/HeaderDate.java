package com.agro.quality_control.data.models;

import android.support.annotation.Nullable;

public class HeaderDate extends RecyclerItem {

    public HeaderDate(@Nullable String pDate) {
        date = pDate;
    }

    @Nullable
    private String date;

    @Nullable
    public String getDate() {
        return date;
    }

    public void setDate(@Nullable String pDate) {
        date = pDate;
    }

    @Override
    public int getType() {
        return RecyclerItem.TYPE_HEADER;
    }
}
