package com.agro.quality_control.data.api;

import android.support.annotation.Nullable;

import com.agro.quality_control.data.models.ReportFile;
import com.agro.quality_control.data.models.Supplier;
import com.agro.quality_control.rx.RxFirebaseFirestore;
import com.agro.quality_control.rx.RxFirebaseStorage;
import com.agro.quality_control.utils.ConstUtils;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Objects;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class FirebaseRepository {

    private FirebaseFirestore mFirebaseFirestore;
    private StorageReference mStorageReference;

    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    @Inject
    FirebaseRepository(FirebaseFirestore pFirebaseFirestore, StorageReference pStorageReference) {
        mFirebaseFirestore = pFirebaseFirestore;
        mStorageReference = pStorageReference;
    }

    public void dispose() {
        mCompositeDisposable.dispose();
    }

    public void getSuppliers(OnRequestListener<Supplier> pOnRequestListener) {
        Disposable disposable = RxFirebaseFirestore.getCollection(
                mFirebaseFirestore,
                ConstUtils.FIRESTORE_COLLECTION_SUPPLIERS,
                null,
                null)
                .subscribeOn(Schedulers.io())
                .subscribe(
                        pQuerySnapshot -> {
                            try {
                                ArrayList<Supplier> result = new ArrayList<>();
                                for (DocumentSnapshot snapshot : pQuerySnapshot.getDocuments()) {
                                    result.add(new Supplier(
                                            snapshot.getId(),
                                            Objects.requireNonNull(snapshot.getString(ConstUtils.PACK_FIELD_NAME)),
                                            Objects.requireNonNull(snapshot.getLong(ConstUtils.PACK_FIELD_ADDED_DATE))));
                                }
                                pOnRequestListener.onSucceed(result);
                            } catch (Exception ex) {
                                pOnRequestListener.onFailed(DataType.GET_SUPPLIERS, ex.getMessage());
                            }
                        }, pThrowable -> {
                            String e = pThrowable.getMessage();
                            pOnRequestListener.onFailed(DataType.GET_SUPPLIERS, e);
                        },
                        () -> pOnRequestListener.onSucceed(new ArrayList<>()));
        mCompositeDisposable.add(disposable);
    }

    public void getReportFileInfo(OnRequestListener<ReportFile> pOnRequestListener) {
        Disposable disposable = RxFirebaseFirestore.getCollection(
                mFirebaseFirestore,
                ConstUtils.FIRESTORE_COLLECTION_REPORTS,
                null,
                null)
                .subscribeOn(Schedulers.io())
                .subscribe(
                        pQuerySnapshot -> {
                            try {
                                ArrayList<ReportFile> result = new ArrayList<>();
                                for (DocumentSnapshot snapshot : pQuerySnapshot.getDocuments()) {
                                    result.add(new ReportFile(
                                            snapshot.getId(),
                                            Objects.requireNonNull(snapshot.getString(ConstUtils.PACK_FIELD_NAME)),
                                            Objects.requireNonNull(snapshot.getString(ConstUtils.PACK_FIELD_URL)),
                                            Objects.requireNonNull(snapshot.getLong(ConstUtils.PACK_FIELD_DATE)),
                                            Objects.requireNonNull(snapshot.getLong(ConstUtils.PACK_FIELD_SIZE))));
                                }
                                pOnRequestListener.onSucceed(result);
                            } catch (Exception ex) {
                                pOnRequestListener.onFailed(DataType.GET_REPORTS, ex.getMessage());
                            }
                        }, pThrowable -> {
                            String e = pThrowable.getMessage();
                            pOnRequestListener.onFailed(DataType.GET_REPORTS, e);
                        },
                        () -> pOnRequestListener.onSucceed(new ArrayList<>()));
        mCompositeDisposable.add(disposable);
    }

    public void setSupplier(String pName, OnRequestListener pOnRequestListener) {
        Supplier supplier = new Supplier(pName);
        Disposable disposable = RxFirebaseFirestore.setDocument(
                mFirebaseFirestore,
                ConstUtils.FIRESTORE_COLLECTION_SUPPLIERS,
                supplier)
                .subscribeOn(Schedulers.io())
                .subscribe(() -> {
                    if (pOnRequestListener != null) {
                        pOnRequestListener.onSucceed(DataType.UPDATE_SUPPLIER);
                    }
                }, pThrowable -> {
                    String e = pThrowable.getMessage();
                    if (pOnRequestListener != null) {
                        pOnRequestListener.onFailed(DataType.UPDATE_SUPPLIER, e);
                    }
                });
        mCompositeDisposable.add(disposable);
    }


    public void deleteSupplier(String pId, OnRequestListener pOnRequestListener) {
        Disposable disposable = RxFirebaseFirestore.deleteDocument(
                mFirebaseFirestore,
                ConstUtils.FIRESTORE_COLLECTION_SUPPLIERS,
                pId)
                .subscribeOn(Schedulers.io())
                .subscribe(() -> {
                    if (pOnRequestListener != null) {
                        pOnRequestListener.onSucceed(DataType.DELETE_SUPPLIER);
                    }
                }, pThrowable -> {
                    String e = pThrowable.getMessage();
                    if (pOnRequestListener != null) {
                        pOnRequestListener.onFailed(DataType.DELETE_SUPPLIER, e);
                    }
                });
        mCompositeDisposable.add(disposable);
    }

    public void updateSupplier(String pId, String pName, OnRequestListener pOnRequestListener) {
        Disposable disposable = RxFirebaseFirestore.updateDocument(
                mFirebaseFirestore,
                ConstUtils.FIRESTORE_COLLECTION_SUPPLIERS,
                pId,
                ConstUtils.PACK_FIELD_NAME,
                pName)
                .subscribeOn(Schedulers.io())
                .subscribe(() -> {
                    if (pOnRequestListener != null) {
                        pOnRequestListener.onSucceed(DataType.UPDATE_SUPPLIER);
                    }
                }, pThrowable -> {
                    String e = pThrowable.getMessage();
                    if (pOnRequestListener != null) {
                        pOnRequestListener.onFailed(DataType.UPDATE_SUPPLIER, e);
                    }
                });
        mCompositeDisposable.add(disposable);
    }

    public void uploadXLS(File pFile, @Nullable OnRequestListener<String> pOnRequestListener) throws FileNotFoundException {
        Disposable disposable = RxFirebaseStorage.uploadFile(mStorageReference, new FileInputStream(pFile), ConstUtils.STORAGE_PATH, pFile.getName() + ConstUtils.PDF)
                .subscribeOn(Schedulers.io())
                .subscribe((pUrl) -> RxFirebaseFirestore.setDocument(
                        mFirebaseFirestore,
                        ConstUtils.FIRESTORE_COLLECTION_REPORTS,
                        new ReportFile(pFile.getName(), pUrl, pFile.length() / ConstUtils.ONE_KB).toMap())
                        .subscribeOn(Schedulers.io())
                        .subscribe(() -> {
                            if (pOnRequestListener != null) {
                                pOnRequestListener.onSucceed(DataType.UPDATE_REPORT);
                            }
                        }, pThrowable -> {
                            String e = pThrowable.getMessage();
                            if (pOnRequestListener != null) {
                                pOnRequestListener.onFailed(DataType.UPDATE_REPORT, e);
                            }
                        }), pThrowable -> {
                    if (pOnRequestListener != null) {
                        pOnRequestListener.onFailed(DataType.UPLOAD_PDF, pThrowable.getMessage());
                    }
                });
        mCompositeDisposable.add(disposable);
    }

    public void deleteXLS(ReportFile pReportFile, @Nullable OnRequestListener pOnRequestListener) {
        Disposable disposable = RxFirebaseStorage.deleteFile(mStorageReference, ConstUtils.STORAGE_PATH + pReportFile.getName())
                .subscribeOn(Schedulers.io())
                .subscribe(() -> RxFirebaseFirestore.deleteDocument(
                        mFirebaseFirestore,
                        ConstUtils.FIRESTORE_COLLECTION_REPORTS,
                        pReportFile.getId())
                        .subscribeOn(Schedulers.io())
                        .subscribe(() -> {
                            if (pOnRequestListener != null) {
                                pOnRequestListener.onSucceed(DataType.DELETE_REPORTS);
                            }
                        }, pThrowable -> {
                            String e = pThrowable.getMessage();
                            if (pOnRequestListener != null) {
                                pOnRequestListener.onFailed(DataType.DELETE_REPORTS, e);
                            }
                        }), pThrowable -> {
                    if (pOnRequestListener != null) {
                        pOnRequestListener.onFailed(DataType.DELETE_PDF, pThrowable.getMessage());
                    }
                });
        mCompositeDisposable.add(disposable);
    }

    public void downloadXLS(File pFile, @Nullable OnRequestListener pOnRequestListener) {
        Disposable disposable = RxFirebaseStorage.downloadFile(mStorageReference, pFile, ConstUtils.STORAGE_PATH, pFile.getName())
                .subscribeOn(Schedulers.io())
                .subscribe((pUrl) -> {
                    if (pOnRequestListener != null) {
                        pOnRequestListener.onSucceed(DataType.DOWNLOAD_PDF);
                    }
                }, pThrowable -> {
                    if (pOnRequestListener != null) {
                        pOnRequestListener.onFailed(DataType.DOWNLOAD_PDF, pThrowable.getMessage());
                    }
                });
        mCompositeDisposable.add(disposable);
    }

}
