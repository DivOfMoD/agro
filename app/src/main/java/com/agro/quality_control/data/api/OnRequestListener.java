package com.agro.quality_control.data.api;

import java.util.ArrayList;

public interface OnRequestListener<T> {

    void onSucceed(ArrayList<T> pObject);

    void onSucceed(T pObject);

    void onSucceed(DataType pDataType);

    void onFailed(DataType pDataType, String pMessage);
}