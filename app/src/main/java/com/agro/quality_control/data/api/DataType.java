package com.agro.quality_control.data.api;

public enum DataType {
    GET_SUPPLIERS,
    GET_REPORTS,
    UPDATE_SUPPLIER,
    UPDATE_REPORT,
    DELETE_SUPPLIER,
    DELETE_REPORTS,

    DOWNLOAD_PDF,
    DELETE_PDF,
    UPLOAD_PDF
}
