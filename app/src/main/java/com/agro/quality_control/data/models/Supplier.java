package com.agro.quality_control.data.models;

import android.support.annotation.NonNull;

import com.google.firebase.firestore.PropertyName;


public class Supplier {

    @NonNull
    private transient String id;
    @NonNull
    private String name;
    @NonNull
    private Long dateAdded;

    public Supplier() {
    }

    public Supplier(@NonNull String pName) {
        name = pName;
        dateAdded = System.currentTimeMillis();
    }

    public Supplier(@NonNull String pId, @NonNull String pName, @NonNull Long pDateAdded) {
        id = pId;
        name = pName;
        dateAdded = pDateAdded;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String pId) {
        id = pId;
    }

    @NonNull
    @PropertyName("name")
    public String getName() {
        return name;
    }

    @PropertyName("name")
    public void setName(@NonNull String pName) {
        name = pName;
    }

    @NonNull
    @PropertyName("date_added")
    public Long getDateAdded() {
        return dateAdded;
    }

    @PropertyName("date_added")
    public void setDateAdded(@NonNull Long pDateAdded) {
        dateAdded = pDateAdded;
    }
}
