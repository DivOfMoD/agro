package com.agro.quality_control.data.models;

import android.support.annotation.NonNull;

import com.agro.quality_control.utils.ConstUtils;
import com.google.firebase.firestore.PropertyName;

import java.util.HashMap;
import java.util.Map;


public class ReportFile {

    @NonNull
    private String id;
    @NonNull
    private String name;
    @NonNull
    private String url;
    @NonNull
    private Long date;
    @NonNull
    private Long size;

    public ReportFile() {
    }

    public ReportFile(@NonNull String pName) {
        name = pName;
    }

    public ReportFile(@NonNull String pName, @NonNull String pUrl, @NonNull Long pSize) {
//        id =
        name = pName;
        url = pUrl;
        size = pSize;
        date = System.currentTimeMillis();
    }

    public ReportFile(@NonNull String pId, @NonNull String pName, @NonNull String pUrl, @NonNull Long pDate, @NonNull Long pSize) {
       id = pId;
        name = pName;
        url = pUrl;
        date = pDate;
        size = pSize;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String pId) {
        id = pId;
    }

    @NonNull
    @PropertyName("name")
    public String getName() {
        return name;
    }

    @PropertyName("name")
    public void setName(@NonNull String pName) {
        name = pName;
    }

    @PropertyName("url")
    @NonNull
    public String getUrl() {
        return url;
    }

    @PropertyName("url")
    public void setUrl(@NonNull String pUrl) {
        url = pUrl;
    }

    @NonNull
    public Long getDate() {
        return date;
    }

    public void setDate(@NonNull Long pDate) {
        date = pDate;
    }

    @NonNull
    public Long getSize() {
        return size;
    }

    public void setSize(@NonNull Long pSize) {
        size = pSize;
    }

    public Map<String, Object> toMap() {
        Map<String, Object> result = new HashMap<>();
        result.put(ConstUtils.PACK_FIELD_NAME, name);
        result.put(ConstUtils.PACK_FIELD_URL, url);
        result.put(ConstUtils.PACK_FIELD_SIZE, size);
        result.put(ConstUtils.PACK_FIELD_DATE, date);
        return result;
    }
}
