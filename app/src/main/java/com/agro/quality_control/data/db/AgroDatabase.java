package com.agro.quality_control.data.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.agro.quality_control.data.db.dao.ReportDataDao;
import com.agro.quality_control.data.models.Report;


@Database(entities = {Report.class}, version = 1)
public abstract class AgroDatabase extends RoomDatabase {

    public static final int SYNC_DRAFT = 0;
    public static final int SYNC_READY = 1;
    public static final int SYNC_DONE = 2;

    public abstract ReportDataDao getReportDao();


}
