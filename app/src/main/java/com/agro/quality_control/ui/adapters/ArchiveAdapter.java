package com.agro.quality_control.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agro.quality_control.R;
import com.agro.quality_control.data.models.ReportFile;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ArchiveAdapter extends RecyclerView.Adapter<ArchiveAdapter.SupplierViewHolder> {

    public interface ArchiveAdapterClickListener {
        void onDeleteClick(ReportFile pReportFile);

        void onShareClick(ReportFile pReportFile);

        void onOpenClick(ReportFile pReportFile);

        void onDownloadClick(ReportFile pReportFile);
    }

    private Context mContext;
    private ArrayList<ReportFile> mReportFileArrayList;

    private ArchiveAdapterClickListener mArchiveAdapterClickListener;

    public ArchiveAdapter(Context pContext) {
        mContext = pContext;
    }

    @NonNull
    @Override
    public SupplierViewHolder onCreateViewHolder(@NonNull ViewGroup container, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(container.getContext());
        View root = inflater.inflate(R.layout.item_archive, container, false);
        return new SupplierViewHolder(mContext, root);
    }

    @Override
    public void onBindViewHolder(@NonNull SupplierViewHolder pViewHolder, int position) {
        ReportFile item = mReportFileArrayList.get(position);
        pViewHolder.setName(item.getName());
        pViewHolder.setDate(item.getDate());
        pViewHolder.setSize(item.getSize());
    }

    @Override
    public int getItemCount() {
        return mReportFileArrayList != null ? mReportFileArrayList.size() : 0;
    }

    public void setAdapterClickListener(ArchiveAdapterClickListener pArchiveAdapterClickListener) {
        mArchiveAdapterClickListener = pArchiveAdapterClickListener;
    }

    public void setData(ArrayList<ReportFile> pSuppliersArrayList) {
        mReportFileArrayList = pSuppliersArrayList;
        notifyDataSetChanged();
    }

    class SupplierViewHolder extends RecyclerView.ViewHolder {

        private Context mContext;

        @BindView(R.id.archive_container)
        ConstraintLayout mSupplierContainer;
        @BindView(R.id.name_text_view)
        TextView mNameTextView;
        @BindView(R.id.date_text_view)
        TextView mDateTextView;
        @BindView(R.id.size_text_view)
        TextView mSizeTextView;

        SupplierViewHolder(Context pContext, @NonNull View itemView) {
            super(itemView);
            mContext = pContext;
            ButterKnife.bind(this, itemView);
        }

        public void setName(String pName) {
            mNameTextView.setText(pName);
        }

        public void setDate(Long pDate) {
            mDateTextView.setText(getFormattedDate(pDate));
        }

        public void setSize(Long pSize) {
            mSizeTextView.setText(mContext.getString(R.string.text_size, pSize));
        }

        private String getFormattedDate(long pTimestamp) {
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
            return formatter.format(new Date(pTimestamp));
        }

        @OnClick(R.id.share_image_view)
        void onShareClick() {
            int position = getAdapterPosition();
            mArchiveAdapterClickListener.onShareClick(mReportFileArrayList.get(position));
        }

        @OnClick(R.id.open_image_view)
        void onOpenClick() {
            int position = getAdapterPosition();
            mArchiveAdapterClickListener.onOpenClick(mReportFileArrayList.get(position));
        }

        @OnClick(R.id.download_image_view)
        void onDownloadClick() {
            int position = getAdapterPosition();
            mArchiveAdapterClickListener.onDownloadClick(mReportFileArrayList.get(position));
        }

        @OnClick(R.id.delete_image_view)
        void onDeleteClick() {
            int position = getAdapterPosition();
            mArchiveAdapterClickListener.onDeleteClick(mReportFileArrayList.get(position));
        }
    }

}