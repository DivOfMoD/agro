package com.agro.quality_control.ui.fragments.reports;

import com.agro.quality_control.data.models.Report;
import com.agro.quality_control.ui.base.BaseModel;
import com.agro.quality_control.ui.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;


public interface ReportsContract {

    interface ReportsView {

        void showError();

        void initData(ArrayList<Report> pReportArrayList);

        void initViews();
    }

    interface ReportsPresenter extends BasePresenter<ReportsView> {

        interface ReportsPresenterListener {
            void onReportsLoaded(ArrayList<Report> pReportArrayList);

            void onReportsDeleted();

            void onArchiveSent();

            void onFailed();
        }

        void formatReport(List<Report> pSelectedItems);

        void loadReports(String pFilter);

        void removeReports(List<Report> pSelectedItems);
    }

    interface ReportsModel extends BaseModel<ReportsPresenter.ReportsPresenterListener> {

        void uploadReport(List<Report> pSelectedItems);

        void getReports(String pFilter);

        void deleteReports(List<Report> pSelectedItems);
    }

}
