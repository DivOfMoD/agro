package com.agro.quality_control.ui.activities.admin;


import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.agro.quality_control.R;
import com.agro.quality_control.ui.NavigationUtils;
import com.agro.quality_control.ui.activities.auth.AuthActivity;
import com.agro.quality_control.ui.base.BaseActivity;
import com.agro.quality_control.ui.fragments.archive.ArchiveFragment;
import com.agro.quality_control.ui.fragments.suppliers.SuppliersFragment;
import com.agro.quality_control.ui.views.ToastUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdminActivity extends BaseActivity implements AdminContract.AdminView, AdminContract.FragmentListener {

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawer;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.navigation_view)
    NavigationView mNavigationView;

    private ActionBarDrawerToggle mDrawerToggle;

    @Inject
    AdminContract.AdminPresenter mAdminPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        ButterKnife.bind(this);
        mAdminPresenter.takeView(this);
    }

    @Override
    public void initViews() {
        setSupportActionBar(mToolbar);
        setupDrawerContent(mNavigationView);
        setupDrawerToggle();
        mDrawer.addDrawerListener(mDrawerToggle);
        this.selectDrawerItem(R.id.nav_archive);
    }

    @Override
    public void navigateAuthPage() {
        NavigationUtils.routeToAppropriatePage(this, AuthActivity.class);
        finish();
    }

    @Override
    public void showSignOutError(String pE) {
        ToastUtils.showStart(this, pE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAdminPresenter.dropView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                menuItem -> {
                    selectDrawerItem(menuItem.getItemId());
                    return true;
                });
    }

    private void selectDrawerItem(int pId) {
        Fragment fragment;
        switch (pId) {
            case R.id.nav_suppliers: {
                fragment = SuppliersFragment.newInstance();
                setTitle(getString(R.string.nav_suppliers));
            }
            break;
            case R.id.nav_exit: {
                mAdminPresenter.signOut();
                mDrawer.closeDrawers();
                return;
            }
            case R.id.nav_archive:
                fragment = ArchiveFragment.newInstance();
                setTitle(getString(R.string.nav_archive));
                break;
            default:
                fragment = SuppliersFragment.newInstance();
                setTitle(getString(R.string.nav_suppliers));
        }

        try {
//            ((BaseFragment) Objects.requireNonNull(fragment)).setMainFragmentListener();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.admin_content, fragment).commit();
        }

        mNavigationView.setCheckedItem(pId);
        mDrawer.closeDrawers();
    }

    private void setupDrawerToggle() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.app_name, R.string.app_name);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

}
