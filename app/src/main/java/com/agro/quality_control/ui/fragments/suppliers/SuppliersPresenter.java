package com.agro.quality_control.ui.fragments.suppliers;

import android.support.annotation.Nullable;

import com.agro.quality_control.data.models.Supplier;

import java.util.ArrayList;

import javax.inject.Inject;

public class SuppliersPresenter implements SuppliersContract.SuppliersPresenter, SuppliersContract.SuppliersPresenter.SuppliersPresenterListener {

    @Nullable
    private SuppliersContract.SuppliersView mSuppliersView;
    private SuppliersModel mSuppliersModel;

    @Inject
    SuppliersPresenter(SuppliersModel pSuppliersModel) {
        mSuppliersModel = pSuppliersModel;
    }

    @Override
    public void takeView(SuppliersContract.SuppliersView view) {
        mSuppliersView = view;
        mSuppliersView.initViews();
        mSuppliersModel.addRequestListener(this);
    }

    @Override
    public void dropView() {
        mSuppliersView = null;
        mSuppliersModel.removeRequestListener();
    }

    @Override
    public void loadSuppliers() {
        mSuppliersModel.getSuppliers();
    }

    @Override
    public void addSupplier(String pName) {
        mSuppliersModel.setSupplier(pName);
    }

    @Override
    public void editSupplier(String pId, String pName) {
        mSuppliersModel.updateSupplier(pId, pName);
    }

    @Override
    public void removeSupplier(String pId) {
        mSuppliersModel.deleteSupplier(pId);
    }

    @Override
    public void onSuppliersLoaded(ArrayList<Supplier> pReportArrayList) {
        if (mSuppliersView != null) {
            mSuppliersView.initData(pReportArrayList);
        }
    }

    @Override
    public void onSupplierDeleted() {
        this.loadSuppliers();
    }

    @Override
    public void onSupplierAdded() {
        this.loadSuppliers();
    }

    @Override
    public void onSupplierEdited() {
        this.loadSuppliers();
    }

    @Override
    public void onFailed() {
        if (mSuppliersView != null) {
            mSuppliersView.showError();
        }
    }

}
