package com.agro.quality_control.ui.activities.permission;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

import com.agro.quality_control.R;
import com.agro.quality_control.ui.activities.auth.AuthActivity;
import com.agro.quality_control.utils.PermissionUtil;

import dagger.android.support.DaggerAppCompatActivity;


public class PermissionRequestActivity extends DaggerAppCompatActivity {
    private static final int PERM_REQUEST_CODE = 435;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permission_request);
    }


    @Override
    protected void onStart() {
        super.onStart();
        requestAllPermissions();
    }

    private void requestAllPermissions() {
        PermissionUtil.requestAllPermissions(this, PERM_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERM_REQUEST_CODE) {
            //make sure permissions request dialog was not interrupted
            if (permissions.length > 0) {
                //check if all permissions were granted
                boolean allPermissionsGranted = true;
                for (int i = 0; i < grantResults.length && allPermissionsGranted; i++) {
                    int grantResult = grantResults[i];
                    allPermissionsGranted = grantResult == PackageManager.PERMISSION_GRANTED;
                }

                if (allPermissionsGranted) {
                    finish();
                    startActivity(new Intent(this, AuthActivity.class));
                } else {
                    boolean canAskForPermissions = true;
                    for (int i = 0; i < permissions.length; i++) {
                        String permission = permissions[i];
                        int grantResult = grantResults[i];

                        boolean needRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
                        boolean isGranted = grantResult == PackageManager.PERMISSION_GRANTED;

                        if (!isGranted && !needRationale) {
                            canAskForPermissions = false;
                            break;
                        }
                    }
                    if (!canAskForPermissions) {
                        setContentView(R.layout.activity_permission_request_settings);
                        findViewById(R.id.settingsButton).setOnClickListener(v -> openSettings());
                    } else {
                        setContentView(R.layout.activity_permission_request_provide);
                        findViewById(R.id.provideButton).setOnClickListener(v -> requestAllPermissions());
                    }
                }
            }
        }
    }

    private void openSettings() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (PermissionUtil.isAllNecessaryPermissionsGranted(this)) {
            super.onBackPressed();
        }
    }
}
