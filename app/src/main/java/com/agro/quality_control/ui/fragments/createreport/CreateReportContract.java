package com.agro.quality_control.ui.fragments.createreport;

import android.os.Parcel;
import android.os.Parcelable;

import com.agro.quality_control.data.models.LocalDropdown;
import com.agro.quality_control.data.models.Report;
import com.agro.quality_control.ui.base.BaseModel;
import com.agro.quality_control.ui.base.BasePresenter;

import java.util.ArrayList;

public interface CreateReportContract {

    enum CreateReportEnum implements Parcelable {
        BOX_NUMBER,
        CULTURE,
        CULTURE_GLUTEN,
        //        CULTURE_DROP,
        CULTURE_YEAR,
        DOC_NUMBER,
        WEIGHT,
        SUPPLIER,
        AUTO_NUMBER,
        HUMIDITY,
        UNLOADING,
        UNLOADING2;

        public static final Parcelable.Creator<CreateReportEnum> CREATOR = new Creator<CreateReportEnum>() {

            @Override
            public CreateReportEnum[] newArray(int size) {
                return new CreateReportEnum[size];
            }

            @Override
            public CreateReportEnum createFromParcel(Parcel source) {
                return CreateReportEnum.values()[source.readInt()];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.ordinal());
        }
    }

    interface CreateReportView {
        void showSaveReportCore(ArrayList<CreateReportContract.CreateReportEnum> pEnumList);

        void showSaveReportWithDraft(ArrayList<CreateReportContract.CreateReportEnum> pEnumList);

        void showCreateReportSucceed();

        void showUpdateReportSucceed();

        void showCreateReportFailed(String pError);

        void initViews();

        void initData(LocalDropdown pCreateReportModel);

        void initSupplierData(ArrayList<String> pSupplierArrayList);

        void initUploading2Data(ArrayList<String> pUnloadingArrayList);
    }

    interface CreateReportPresenter extends BasePresenter<CreateReportView> {

        interface CreateReportPresenterListener {
            void onReportCreated();

            void onReportUpdated();

            void onFailed(String pMessage);

            void onAPIDataLoaded(LocalDropdown pCreateReportModel);

            void onAPIDataLoaded(ArrayList<String> pSupplierArrayList);

            void onUnloading2DataLoaded(ArrayList<String> pUnloading2ArrayList);
        }

        void initViews();

        void getData();

        void createReport(Report pReport);

        void updateReport(Report pReport);

        void createDraftReport(Report pReport);

        void updateDraftReport(Report pReport);

        void getUnloading2Data(int pPosition);
    }

    interface CreateReportModel extends BaseModel<CreateReportPresenter.CreateReportPresenterListener> {
        void createReport(Report pReport);

        void updateReport(Report pReport);

        void getDropdownLocalData();

        void getDropdownServerData();

        void getUnloading2Data(int pPosition);
    }
}
