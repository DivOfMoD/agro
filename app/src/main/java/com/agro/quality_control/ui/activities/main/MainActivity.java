package com.agro.quality_control.ui.activities.main;


import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.agro.quality_control.R;
import com.agro.quality_control.ui.activities.auth.AuthActivity;
import com.agro.quality_control.ui.base.BaseActivity;
import com.agro.quality_control.data.models.Report;
import com.agro.quality_control.ui.base.BaseFragment;
import com.agro.quality_control.ui.fragments.archive.ArchiveFragment;
import com.agro.quality_control.ui.fragments.createreport.CreateReportFragment;
import com.agro.quality_control.ui.fragments.reports.ReportsFragment;
import com.agro.quality_control.ui.NavigationUtils;
import com.agro.quality_control.ui.views.ToastUtils;

import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements com.agro.quality_control.ui.activities.main.MainContract.MainView, com.agro.quality_control.ui.activities.main.MainContract.FragmentListener {

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawer;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.navigation_view)
    NavigationView mNavigationView;

    private ActionBarDrawerToggle mDrawerToggle;

    @Inject
    com.agro.quality_control.ui.activities.main.MainContract.MainPresenter mMainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mMainPresenter.takeView(this);
    }

    @Override
    public void initViews() {
        setSupportActionBar(mToolbar);
        setupDrawerContent(mNavigationView);
        setupDrawerToggle();
        mDrawer.addDrawerListener(mDrawerToggle);
        this.selectDrawerItem(R.id.nav_create_report, null);
    }

    @Override
    public void navigateAuthPage() {
        NavigationUtils.routeToAppropriatePage(this, AuthActivity.class);
        finish();
    }

    @Override
    public void showSignOutError(String pE) {
        ToastUtils.showStart(this, pE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMainPresenter.dropView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                menuItem -> {
                    selectDrawerItem(menuItem.getItemId(), null);
                    return true;
                });
    }

    private void selectDrawerItem(int pId, @Nullable Report pReport) {
        Fragment fragment;
        switch (pId) {
            case R.id.nav_create_report: {
                if (pReport != null) {
                    fragment = CreateReportFragment.newInstance(pReport);
                    setTitle(getString(R.string.nav_edit_report));
                } else {
                    fragment = CreateReportFragment.newInstance();
                    setTitle(getString(R.string.nav_create_report));
                }
            }
            break;
            case R.id.nav_daily_reports:
                fragment = ReportsFragment.newInstance();
                setTitle(getString(R.string.nav_daily_reports));
                break;
            case R.id.nav_exit: {
                mMainPresenter.signOut();
                mDrawer.closeDrawers();
                return;
            }
            case R.id.nav_archive:
                fragment = ArchiveFragment.newInstance();
                setTitle(getString(R.string.nav_archive));
                break;
            default:
                fragment = CreateReportFragment.newInstance();
                setTitle(getString(R.string.nav_create_report));
        }

        try {
            ((BaseFragment) Objects.requireNonNull(fragment)).setMainFragmentListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.main_content, fragment).commit();
        }

        mNavigationView.setCheckedItem(pId);
        mDrawer.closeDrawers();
    }

    private void setupDrawerToggle() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.app_name, R.string.app_name);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onReportClick(Report pReport) {
        this.selectDrawerItem(R.id.nav_create_report, pReport);
    }
}
