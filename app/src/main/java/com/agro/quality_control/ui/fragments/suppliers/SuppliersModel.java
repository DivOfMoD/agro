package com.agro.quality_control.ui.fragments.suppliers;

import com.agro.quality_control.data.api.DataType;
import com.agro.quality_control.data.api.FirebaseRepository;
import com.agro.quality_control.data.api.OnRequestListener;
import com.agro.quality_control.data.models.Supplier;

import java.util.ArrayList;

import javax.inject.Inject;

public class SuppliersModel implements SuppliersContract.SuppliersModel {

    private FirebaseRepository mFirebaseRepository;
    private SuppliersContract.SuppliersPresenter.SuppliersPresenterListener mSuppliersPresenterListener;

    @Inject
    SuppliersModel(FirebaseRepository pFirebaseRepository) {
        mFirebaseRepository = pFirebaseRepository;
    }

    @Override
    public void removeRequestListener() {
        mSuppliersPresenterListener = null;
    }

    @Override
    public void addRequestListener(SuppliersContract.SuppliersPresenter.SuppliersPresenterListener pListener) {
        mSuppliersPresenterListener = pListener;
    }

    @Override
    public void setSupplier(String pName) {
        mFirebaseRepository.setSupplier(pName, new OnRequestListener() {
            @Override
            public void onSucceed(ArrayList pObject) {
                //TODO NOT CALLED
            }

            @Override
            public void onSucceed(Object pObject) {

            }

            @Override
            public void onSucceed(DataType pDataType) {
                mSuppliersPresenterListener.onSupplierAdded();
            }

            @Override
            public void onFailed(DataType pDataType, String pMessage) {
                mSuppliersPresenterListener.onFailed();
            }
        });
    }

    @Override
    public void updateSupplier(String pId, String pName) {
        mFirebaseRepository.updateSupplier(pId, pName, new OnRequestListener() {
            @Override
            public void onSucceed(ArrayList pObject) {
                //TODO NOT CALLED
            }

            @Override
            public void onSucceed(Object pObject) {

            }

            @Override
            public void onSucceed(DataType pDataType) {
                mSuppliersPresenterListener.onSupplierEdited();
            }

            @Override
            public void onFailed(DataType pDataType, String pMessage) {
                mSuppliersPresenterListener.onFailed();
            }
        });
    }

    @Override
    public void deleteSupplier(String pId) {
        mFirebaseRepository.deleteSupplier(pId, new OnRequestListener() {
            @Override
            public void onSucceed(ArrayList pObject) {
                //TODO NOT CALLED
            }

            @Override
            public void onSucceed(Object pObject) {

            }

            @Override
            public void onSucceed(DataType pDataType) {
                mSuppliersPresenterListener.onSupplierDeleted();
            }

            @Override
            public void onFailed(DataType pDataType, String pMessage) {
                mSuppliersPresenterListener.onFailed();
            }
        });
    }

    @Override
    public void getSuppliers() {
        mFirebaseRepository.getSuppliers(new OnRequestListener<Supplier>() {
            @Override
            public void onSucceed(ArrayList<Supplier> pObject) {
                mSuppliersPresenterListener.onSuppliersLoaded(pObject);
            }

            @Override
            public void onSucceed(Supplier pObject) {

            }

            @Override
            public void onSucceed(DataType pDataType) {
                //TODO NOT CALLED
            }

            @Override
            public void onFailed(DataType pDataType, String pMessage) {
                mSuppliersPresenterListener.onFailed();
            }
        });
    }

}
