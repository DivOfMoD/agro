package com.agro.quality_control.ui.activities.main;

import android.support.annotation.Nullable;

import javax.inject.Inject;

public class MainPresenter implements MainContract.MainPresenter, MainContract.MainModel.OnSignOutStateListener {

    @Nullable
    private MainContract.MainView mMainView;
    private MainModel mMainModel;

    @Inject
    public MainPresenter(MainModel pMainModel) {
        mMainModel = pMainModel;
    }

    @Override
    public void takeView(MainContract.MainView view) {
        mMainView = view;
        mMainView.initViews();
        mMainModel.addSignOutStateListener(this);
    }

    @Override
    public void dropView() {
        mMainView = null;
        mMainModel.removeSignOutStateListener();
    }

    @Override
    public void signOut() {
        mMainModel.signOut();
    }

    @Override
    public void onSuccess() {
        if (mMainView != null) {
            mMainView.navigateAuthPage();
        }
    }

    @Override
    public void onFailed(Exception pE) {
        if (mMainView != null) {

        }
    }
}
