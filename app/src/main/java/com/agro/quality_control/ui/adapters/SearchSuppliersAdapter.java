package com.agro.quality_control.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agro.quality_control.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SearchSuppliersAdapter extends RecyclerView.Adapter<SearchSuppliersAdapter.SupplierViewHolder> {

    public interface SuppliersAdapterClickListener {
        void onReportClick(String pSupplier);
    }

    private Context mContext;
    private ArrayList<String> mSuppliersArrayList;

    private SuppliersAdapterClickListener mSuppliersAdapterClickListener;

    public SearchSuppliersAdapter(Context pContext) {
        mContext = pContext;
    }

    @NonNull
    @Override
    public SupplierViewHolder onCreateViewHolder(@NonNull ViewGroup container, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(container.getContext());
        View root = inflater.inflate(R.layout.item_supplier_search, container, false);
        return new SupplierViewHolder(mContext, root);
    }

    @Override
    public void onBindViewHolder(@NonNull SupplierViewHolder pViewHolder, int position) {
        String item = mSuppliersArrayList.get(position);
        pViewHolder.setSupplier(item);
    }

    @Override
    public int getItemCount() {
        return mSuppliersArrayList != null ? mSuppliersArrayList.size() : 0;
    }

    public void setAdapterClickListener(SuppliersAdapterClickListener pSuppliersAdapterClickListener) {
        mSuppliersAdapterClickListener = pSuppliersAdapterClickListener;
    }

    public void setData(ArrayList<String> pSuppliersArrayList) {
        mSuppliersArrayList = pSuppliersArrayList;
        notifyDataSetChanged();
    }

    class SupplierViewHolder extends RecyclerView.ViewHolder {

        private Context mContext;

        @BindView(R.id.supplier_container)
        ConstraintLayout mSupplierContainer;
        @BindView(R.id.supplier_text_view)
        TextView mSupplierTextView;

        public SupplierViewHolder(Context pContext, @NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setSupplier(String pSupplier) {
            mSupplierTextView.setText(pSupplier);
        }

        @OnClick(R.id.supplier_container)
        void onClick(View pView) {
            int position = getAdapterPosition();
            mSuppliersAdapterClickListener.onReportClick(mSuppliersArrayList.get(position));
        }
    }

}