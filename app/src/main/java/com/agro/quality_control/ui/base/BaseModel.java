package com.agro.quality_control.ui.base;

public interface BaseModel<T> {

    void removeRequestListener();

    void addRequestListener(T pListener);
}

