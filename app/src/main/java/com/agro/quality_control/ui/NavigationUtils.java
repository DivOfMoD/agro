package com.agro.quality_control.ui;

import android.app.Activity;
import android.content.Intent;

import com.agro.quality_control.R;

public abstract class NavigationUtils {

    public static void routeToAppropriatePage(Activity pActivity, Class<?> pClass) {
        pActivity.startActivity(new Intent(pActivity, pClass));
        pActivity.overridePendingTransition(R.anim.fade_in, R.anim.no_fade);
    }

}
