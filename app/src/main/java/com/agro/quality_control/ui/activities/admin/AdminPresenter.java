package com.agro.quality_control.ui.activities.admin;

import android.support.annotation.Nullable;

import javax.inject.Inject;

public class AdminPresenter implements AdminContract.AdminPresenter, AdminContract.AdminModel.OnSignOutStateListener {

    @Nullable
    private AdminContract.AdminView mAdminView;
    private AdminModel mAdminModel;

    @Inject
    public AdminPresenter(AdminModel pAdminModel) {
        mAdminModel = pAdminModel;
    }

    @Override
    public void takeView(AdminContract.AdminView view) {
        mAdminView = view;
        mAdminView.initViews();
        mAdminModel.addSignOutStateListener(this);
    }

    @Override
    public void dropView() {
        mAdminView = null;
        mAdminModel.removeSignOutStateListener();
    }

    @Override
    public void signOut() {
        mAdminModel.signOut();
    }

    @Override
    public void onSuccess() {
        if (mAdminView != null) {
            mAdminView.navigateAuthPage();
        }
    }

    @Override
    public void onFailed(Exception pE) {
        if (mAdminView != null) {

        }
    }
}
