package com.agro.quality_control.ui.views;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;

import com.agro.quality_control.R;
import com.agro.quality_control.ui.adapters.DropdownAdapter;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Dmitry Ushkevich on 27.06.18.
 */
public class CustomSpinner extends android.support.v7.widget.AppCompatSpinner {

    public CustomSpinner(Context context) {
        super(context);
    }

    public CustomSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomSpinner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setSelection(int position, boolean animate) {
        boolean sameSelected = position == getSelectedItemPosition();
        super.setSelection(position, animate);
        if (sameSelected) {
            if (getOnItemSelectedListener() != null)
                getOnItemSelectedListener().onItemSelected(this, getSelectedView(), position, getSelectedItemId());
        }
    }

    @Override
    public void setSelection(int position) {
        boolean sameSelected = position == getSelectedItemPosition();
        super.setSelection(position);
        if (sameSelected) {
            if (getOnItemSelectedListener() != null)
                getOnItemSelectedListener().onItemSelected(this, getSelectedView(), position, getSelectedItemId());
        }
    }

    public void setSelection(String pSelection) {
        int position = getPosition(pSelection);
        if (position != -1) {
            super.setSelection(position);
        }
    }

    public void setSelectAdapter(Activity activity, String text) {
        DropdownAdapter<String> adapter = new DropdownAdapter<>(activity, R.layout.item_dropdown_text, R.id.spinner_text, Collections.singletonList(text));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.setAdapter(adapter);
    }

    public void setSelectAdapter(Activity activity, ArrayList<String> vars) {
        DropdownAdapter<String> adapter = new DropdownAdapter<>(activity, R.layout.item_dropdown_text, R.id.spinner_text, vars);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.setAdapter(adapter);
    }

    public String getValue() {
        String result = null;
        try {
            int i = getSelectedItemPosition();
            result = (String) getAdapter().getItem(i);
        } catch (Exception ex) {

        }
        return result;
    }

    public int getCurrentPosition(){
        return super.getSelectedItemPosition();
    }

    private int getPosition(String pItem) {
        for (int i = 0; i < getAdapter().getCount(); i++) {
            if (getAdapter().getItem(i).equals(pItem)) {
                return i;
            }
        }
        return -1;
    }

    private ArrayList<String> getValues() {
        ArrayList<String> result = new ArrayList<>();
        for (int i = 0; i < getAdapter().getCount(); i++) {
            result.add((String) getAdapter().getItem(i));
        }
        return result;
    }

}
