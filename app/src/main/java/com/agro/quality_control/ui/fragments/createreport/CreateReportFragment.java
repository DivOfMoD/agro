package com.agro.quality_control.ui.fragments.createreport;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.agro.quality_control.R;
import com.agro.quality_control.data.models.LocalDropdown;
import com.agro.quality_control.data.models.Report;
import com.agro.quality_control.ui.base.BaseFragment;
import com.agro.quality_control.ui.views.CustomDialog;
import com.agro.quality_control.ui.views.DropdownList;
import com.agro.quality_control.ui.views.ToastUtils;

import java.util.ArrayList;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CreateReportFragment extends BaseFragment implements CreateReportContract.CreateReportView {

    private static final String REPORT_ARGS = "REPORT_ARGS";
    private Report mReport;

    @BindView(R.id.supplier_view)
    TextView mSupplierView;
    @BindView(R.id.title_unloading2_text_view)
    TextView mUnloading2TextView;

    @BindView(R.id.culture_dropdown_list)
    DropdownList mCultureDropdownList;
    @BindView(R.id.culture_year_dropdown_list)
    DropdownList mCultureYearDropdownList;
    @BindView(R.id.garbage_dropdown_list)
    DropdownList mGarbageDropdownList;
    @BindView(R.id.smell_dropdown_list)
    DropdownList mSmellDropdownList;
    @BindView(R.id.unloading_dropdown_list)
    DropdownList mUnloadingDropdownList;
    @BindView(R.id.unloading2_dropdown_list)
    DropdownList mUnloading2DropdownList;

    @BindView(R.id.box_number_edit_text)
    EditText mBoxNumberEditText;
    @BindView(R.id.doc_number_edit_text)
    EditText mDocNumberEditText;
    @BindView(R.id.weight_number_edit_text)
    EditText mWeightEditText;
    @BindView(R.id.loading_edit_text)
    EditText mLoadingEditText;
    @BindView(R.id.auto_number_edit_text)
    EditText mAutoNumberEditText;
    @BindView(R.id.humidity_edit_text)
    EditText mHumidityEditText;
    @BindView(R.id.unloading2_number_edit_text)
    EditText mUnloading2EditText;
    @BindView(R.id.gluten_number_edit_text)
    EditText mGlutenEditText;
    @BindView(R.id.gluten_idk_number_edit_text)
    EditText mGlutenIdkEditText;
    @BindView(R.id.nature_number_edit_text)
    EditText mNatureEditText;
    @BindView(R.id.drop_number_edit_text)
    EditText mDropEditText;

    @BindView(R.id.box_number_text_input_layout)
    TextInputLayout mBoxNumberLayout;
    @BindView(R.id.doc_number_text_input_layout)
    TextInputLayout mDocNumberLayout;
    @BindView(R.id.weight_text_input_layout)
    TextInputLayout mWeightLayout;
    @BindView(R.id.auto_number_text_input_layout)
    TextInputLayout mAutoNumberLayout;
    @BindView(R.id.humidity_text_input_layout)
    TextInputLayout mHumidityLayout;
    @BindView(R.id.unloading2_input_layout)
    TextInputLayout mUnloading2Layout;
    @BindView(R.id.gluten_input_layout)
    TextInputLayout mGlutenLayout;
    @BindView(R.id.gluten_idk_input_layout)
    TextInputLayout mGlutenIdkLayout;
    @BindView(R.id.nature_input_layout)
    TextInputLayout mNatureLayout;
    @BindView(R.id.drop_input_layout)
    TextInputLayout mDropLayout;

    @BindView(R.id.poison_switch)
    Switch mPoisonSwitch;

    @Inject
    CreateReportContract.CreateReportPresenter mCreateReportPresenter;

    private CustomDialog mDialog;
    private ArrayList<String> mSuppliersArrayList;

    public static CreateReportFragment newInstance() {
        return new CreateReportFragment();
    }

    public static CreateReportFragment newInstance(Report pReport) {
        CreateReportFragment reportFragment = new CreateReportFragment();
        Bundle args = new Bundle();
        args.putParcelable(REPORT_ARGS, pReport);

        reportFragment.setArguments(args);
        return reportFragment;
    }

    public CreateReportFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_report, container, false);
        ButterKnife.bind(this, view);
        if (getArguments() != null) {
            mReport = getArguments().getParcelable(REPORT_ARGS);
        }
        mCreateReportPresenter.takeView(this);
        mCreateReportPresenter.initViews();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mCreateReportPresenter.dropView();
    }

    @Override
    public void showSaveReportCore(ArrayList<CreateReportContract.CreateReportEnum> pEnumList) {
        openSaveReportCoreDialog(pEnumList);
    }

    @Override
    public void showSaveReportWithDraft(ArrayList<CreateReportContract.CreateReportEnum> pEnumList) {
        openSaveReportDraftDialog(pEnumList);
    }

    @UiThread
    @Override
    public void showCreateReportSucceed() {
        Objects.requireNonNull(getActivity()).runOnUiThread(() -> {
            clearFields();
            ToastUtils.showStart(getActivity(), getString(R.string.report_created));
        });
    }

    @Override
    public void showUpdateReportSucceed() {
        Objects.requireNonNull(getActivity()).runOnUiThread(() -> {
            clearFields();
            ToastUtils.showStart(getActivity(), getString(R.string.report_updated));
        });
    }

    @UiThread
    @Override
    public void showCreateReportFailed(String pError) {
        Objects.requireNonNull(getActivity()).runOnUiThread(() ->
                ToastUtils.showStart(getActivity(), pError));
    }

    @Override
    public void initViews() {
        Objects.requireNonNull(getActivity()).getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        mCreateReportPresenter.getData();
    }

    @Override
    public void initData(LocalDropdown pCreateReportModel) {
        mCultureYearDropdownList.setSelectAdapter(getActivity(), pCreateReportModel.getCultureYearArrayList(), null);
        mGarbageDropdownList.setSelectAdapter(getActivity(), pCreateReportModel.getGarbageArrayList(), null);
        mSmellDropdownList.setSelectAdapter(getActivity(), pCreateReportModel.getSmellArrayList(), null);
        mCultureDropdownList.setSelectAdapter(getActivity(), pCreateReportModel.getCultureArrayList(), (pPosition -> {
            switch (pPosition) {
                case 0:
                case 2:
                case 4: {
                    mGlutenLayout.setVisibility(View.GONE);
                    mGlutenIdkLayout.setVisibility(View.GONE);
                    mNatureLayout.setVisibility(View.GONE);
                    mDropLayout.setVisibility(View.GONE);
                }
                break;
                case 1: {
                    mGlutenLayout.setVisibility(View.VISIBLE);
                    mGlutenIdkLayout.setVisibility(View.VISIBLE);
                    mNatureLayout.setVisibility(View.VISIBLE);
                    mDropLayout.setVisibility(View.VISIBLE);
                }
                break;
                case 3:
                case 5: {
                    mGlutenLayout.setVisibility(View.GONE);
                    mGlutenIdkLayout.setVisibility(View.GONE);
                    mNatureLayout.setVisibility(View.VISIBLE);
                    mDropLayout.setVisibility(View.VISIBLE);
                }
                break;
            }
        }));
        mUnloadingDropdownList.setSelectAdapter(getActivity(), pCreateReportModel.getUnloadingArrayList(), (pPosition -> {
            switch (pPosition) {
                case 0: {
                    this.clearUnloading2();
                }
                break;
                case 1:
                case 5:
                case 6: {
                    mUnloading2Layout.setVisibility(View.GONE);
                    mUnloading2TextView.setVisibility(View.VISIBLE);
                    mUnloading2DropdownList.setVisibility(View.VISIBLE);
                    mUnloading2TextView.setText(pPosition == 1 ? R.string.title_shop : R.string.title_barrel);
                    mCreateReportPresenter.getUnloading2Data(pPosition);
                }
                break;
                case 2:
                case 3:
                case 4: {
                    mUnloading2TextView.setVisibility(View.GONE);
                    mUnloading2DropdownList.setVisibility(View.GONE);
                    mUnloading2Layout.setVisibility(View.VISIBLE);
                    if (mReport != null) {
                        mUnloading2EditText.setText(mReport.getUnloading2());
                    }
                }
                break;
            }
        }));

        if (mReport != null) {
            mCultureDropdownList.setSelection(mReport.getCulture());
            mCultureYearDropdownList.setSelection(mReport.getCultureYear());
            mGarbageDropdownList.setSelection(mReport.getGarbage());
            mSmellDropdownList.setSelection(mReport.getSmell());
            mUnloadingDropdownList.setSelection(mReport.getUnloading());

            mBoxNumberEditText.setText(mReport.getBoxNumber());
            mDocNumberEditText.setText(mReport.getDocNumber());
            mWeightEditText.setText(mReport.getWeight());
            mAutoNumberEditText.setText(mReport.getAutoNumber());
            mLoadingEditText.setText(mReport.getLoading());
            mHumidityEditText.setText(mReport.getHumidity());
            mGlutenEditText.setText(mReport.getGluten());
            mGlutenIdkEditText.setText(mReport.getGlutenIdk());
            mNatureEditText.setText(mReport.getNature());
            mDropEditText.setText(mReport.getDrop());

            mPoisonSwitch.setChecked(mReport.isPoison());

            if (mReport.getUnloading2() != null) {
                int unloadingPosition = mUnloadingDropdownList.getCurrentPosition();
                mCreateReportPresenter.getUnloading2Data(unloadingPosition);
            }
        }
    }


    @Override
    public void initSupplierData(ArrayList<String> pSuppliersList) {
        mSuppliersArrayList = pSuppliersList;
        if (mReport != null) {
            mSupplierView.setText(mReport.getSupplier());
        }
    }

    @Override
    public void initUploading2Data(ArrayList<String> pUnloadingArrayList) {
        mUnloading2DropdownList.setSelectAdapter(getActivity(), pUnloadingArrayList, null);

        if (mReport != null) {
            String unloading2Value = mReport.getUnloading2();
            if (mUnloadingDropdownList.getUnloading2Type() == DropdownList.TYPE_DROPDOWN) {
                mUnloading2DropdownList.setSelection(unloading2Value);
            } else {
                mUnloading2EditText.setText(unloading2Value);
            }
        }
    }

    @OnClick(R.id.save_text_view)
    void save() {
        this.hideKeyboard();
        this.clearErrors();
        Report report = getReport();

        if (report.getId() == 0) {
            mCreateReportPresenter.createReport(report);
        } else {
            mCreateReportPresenter.updateReport(report);
        }
    }

    private Report getReport() {

        String unloading2;
        if (mUnloadingDropdownList.getUnloading2Type() == DropdownList.TYPE_DROPDOWN) {
            unloading2 = mUnloading2DropdownList.getValue();
        } else {
            unloading2 = mUnloading2EditText.getText().toString();
        }

        Report result = new Report(
                mBoxNumberEditText.getText().toString(),
                mCultureDropdownList.getValue(),
                mGlutenEditText.getText().toString(),
                mGlutenIdkEditText.getText().toString(),
                mNatureEditText.getText().toString(),
                mDropEditText.getText().toString(),
                mCultureYearDropdownList.getValue(),
                mDocNumberEditText.getText().toString(),
                mWeightEditText.getText().toString(),
                mSupplierView.getText().toString(),
                mLoadingEditText.getText().toString(),
                mAutoNumberEditText.getText().toString(),
                mHumidityEditText.getText().toString(),
                mPoisonSwitch.isChecked(),
                mGarbageDropdownList.getValue(),
                mSmellDropdownList.getValue(),
                mUnloadingDropdownList.getValue(),
                unloading2
        );

        if (mReport == null) {
            return result;
        } else {
            result.setId(Objects.requireNonNull(mReport).getId());
            return result;
        }
    }


    private void showErrors(ArrayList<CreateReportContract.CreateReportEnum> pEnumList) {
        for (CreateReportContract.CreateReportEnum item : pEnumList) {
            switch (item) {
                case BOX_NUMBER: {
                    mBoxNumberLayout.setError(" ");
                }
                break;
                case CULTURE: {
                    this.changeViewForeground(R.drawable.bg_rounded_red_button, mCultureDropdownList);
                }
                break;
                case CULTURE_GLUTEN: {
                    mGlutenLayout.setError(" ");
                }
                break;
//                case CULTURE_DROP: {
//                    mDropLayout.setError(" ");
//                }
//                break;
                case CULTURE_YEAR: {
                    this.changeViewForeground(R.drawable.bg_rounded_red_button, mCultureYearDropdownList);
                }
                break;
                case DOC_NUMBER: {
                    mDocNumberLayout.setError(" ");
                }
                case WEIGHT: {
                    mWeightLayout.setError(" ");
                }
                break;
                case SUPPLIER: {
                    this.changeViewForeground(R.drawable.bg_rounded_red_button, mSupplierView);
                }
                break;
                case AUTO_NUMBER: {
                    mAutoNumberLayout.setError(" ");
                }
                break;
                case HUMIDITY: {
                    mHumidityLayout.setError(" ");
                }
                break;
                case UNLOADING: {
                    this.changeViewForeground(R.drawable.bg_rounded_red_button, mUnloadingDropdownList);
                }
                break;
                case UNLOADING2: {
                    if (mUnloadingDropdownList.getUnloading2Type() == DropdownList.TYPE_DROPDOWN) {
                        this.changeViewForeground(R.drawable.bg_rounded_red_button, mUnloading2DropdownList);
                    } else {
                        mUnloading2Layout.setError(" ");
                    }
                }
                break;
            }
        }
    }

    private void clearErrors() {
        this.changeViewForeground(R.drawable.bg_rounded_dark_button, mCultureDropdownList);
        this.changeViewForeground(R.drawable.bg_rounded_dark_button, mCultureYearDropdownList);
        this.changeViewForeground(R.drawable.bg_rounded_dark_button, mSupplierView);
        this.changeViewForeground(R.drawable.bg_rounded_dark_button, mUnloadingDropdownList);
        this.changeViewForeground(R.drawable.bg_rounded_dark_button, mUnloading2DropdownList);
        mBoxNumberLayout.setError(null);
        mDocNumberLayout.setError(null);
        mWeightLayout.setError(null);
        mAutoNumberLayout.setError(null);
        mHumidityLayout.setError(null);
        mUnloading2Layout.setError(null);
        mGlutenLayout.setError(null);
        mDropLayout.setError(null);
    }


    void clearFields() {
        mBoxNumberEditText.setText("");
        mDocNumberEditText.setText("");
        mWeightEditText.setText("");
        mAutoNumberEditText.setText("");
        mLoadingEditText.setText("");
        mHumidityEditText.setText("");
        mUnloading2EditText.setText("");
        mGlutenEditText.setText("");
        mGlutenIdkEditText.setText("");
        mNatureEditText.setText("");
        mDropEditText.setText("");
        this.clearUnloading2();
        this.clearErrors();
        this.hideKeyboard();
    }

    void clearUnloading2() {
        mUnloading2Layout.setVisibility(View.GONE);
        mUnloading2TextView.setVisibility(View.GONE);
        mUnloading2DropdownList.setVisibility(View.GONE);
    }

    void hideKeyboard() {
        Context context = getContext();
        if (context != null) {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
            View view = getView();
            if (imm != null && view != null) {
                imm.hideSoftInputFromWindow(view.getRootView().getWindowToken(), 0);
            }
        }
    }

    private void changeViewForeground(@DrawableRes int pId, View pView) {
        pView.setForeground(getResources().getDrawable(pId,
                Objects.requireNonNull(getContext()).getTheme()));
    }

    void openSaveReportCoreDialog(
            final ArrayList<CreateReportContract.CreateReportEnum> pEnumList) {

        closeDialog();

        mDialog = CustomDialog.newInstance(CustomDialog.DIALOG_SAVE_REPORT_CORE, pEnumList, new CustomDialog.OnYesNoDialogClickListener() {
            @Override
            public void onYesClick() {

            }

            @Override
            public void onNoClick() {
                closeDialog();
                showErrors(pEnumList);
            }
        });

        mDialog.show(getFragmentManager(), CustomDialog.TAG);
    }

    void openSaveReportDraftDialog(
            final ArrayList<CreateReportContract.CreateReportEnum> pEnumList) {

        closeDialog();

        mDialog = CustomDialog.newInstance(CustomDialog.DIALOG_SAVE_REPORT_DRAFT, null, new CustomDialog.OnYesNoDialogClickListener() {
            @Override
            public void onYesClick() {
                Report report = getReport();

                if (report.getId() == 0) {
                    mCreateReportPresenter.createDraftReport(report);
                } else {
                    mCreateReportPresenter.updateDraftReport(report);
                }

                closeDialog();
            }

            @Override
            public void onNoClick() {
                closeDialog();
                showErrors(pEnumList);
            }
        });

        mDialog.show(getFragmentManager(), CustomDialog.TAG);
    }

    @OnClick(R.id.supplier_view)
    void openSuppliersDialog(View pView) {

        closeDialog();

        mDialog = CustomDialog.newInstance(CustomDialog.DIALOG_SUPPLIER, mSuppliersArrayList, new CustomDialog.OnSupplierDialogClickListener() {
            @Override
            public void onCloseClick() {
                closeDialog();
            }

            @Override
            public void onSupplierClick(String pSupplier) {
                mSupplierView.setText(pSupplier);
            }
        });

        mDialog.show(getFragmentManager(), CustomDialog.TAG);
    }

    void closeDialog() {
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
    }

}
