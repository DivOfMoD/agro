package com.agro.quality_control.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

public class DropdownAdapter<T> extends ArrayAdapter {

    private Context mContext;

    public DropdownAdapter(@NonNull Context context, int resource, @NonNull Object[] objects) {
        super(context, resource, Arrays.asList(objects));
        mContext = context;
    }

    public DropdownAdapter(@NonNull Context context, int resource, int textViewResourceId, @NonNull List objects) {
        super(context, resource, textViewResourceId, objects);
        mContext = context;
    }

    @Override
    public View getDropDownView(final int position, View convertView, @NonNull ViewGroup parent) {

        if (convertView == null) {
            convertView = new TextView(mContext);
        }

        TextView textView = (TextView) convertView;
//
//        if (getItem(position) instanceof SelectVars) {
//            SelectVars item = (SelectVars) getItem(position);
//            textView.setText(item != null ? item.getSelectTitle() : null);
//            textView.setTextSize(18);
//        } else if (getItem(position) instanceof String) {
        String item = (String) getItem(position);
        textView.setText(item);
        textView.setTextSize(22);
        textView.setPadding(40, 5, 5, 5);
//        }

//        textView.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimaryDark));

//        final TextView finalItem = textView;
//        textView.post(() -> finalItem.setSingleLine(false));
        return convertView;
    }

}