package com.agro.quality_control.ui.fragments.reports;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.agro.quality_control.R;
import com.agro.quality_control.data.models.Report;
import com.agro.quality_control.ui.adapters.ReportsAdapter;
import com.agro.quality_control.ui.base.BaseFragment;
import com.agro.quality_control.ui.views.CustomDialog;
import com.agro.quality_control.ui.views.InsetDecoration;

import java.util.ArrayList;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ReportsFragment extends BaseFragment implements ReportsContract.ReportsView, ReportsAdapter.ReportsAdapterClickListener, CompoundButton.OnCheckedChangeListener {

    @BindView(R.id.choose_all_check_box)
    CheckBox mChooseAllCheckBox;
    @BindView(R.id.no_data_text_view)
    TextView mNoDataTextView;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.reports_recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.search_edit_text)
    EditText mSearchEditText;
    @BindView(R.id.view_search)
    View mSearchView;

    @Inject
    ReportsContract.ReportsPresenter mReportsPresenter;
    @Inject
    InsetDecoration mInsetDecoration;
    @Inject
    LinearLayoutManager mLinearLayoutManager;
    @Inject
    ReportsAdapter mReportsAdapter;

    private CustomDialog mDialog;

    public static ReportsFragment newInstance() {
        return new ReportsFragment();
    }

    public ReportsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reports, container, false);
        ButterKnife.bind(this, view);
        mReportsPresenter.takeView(this);
        loadData("");
        return view;
    }

    @Override
    public void initViews() {
        mChooseAllCheckBox.setOnCheckedChangeListener(this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.addItemDecoration(mInsetDecoration);
        mRecyclerView.setAdapter(mReportsAdapter);
        mReportsAdapter.setReportsAdapterClickListener(this);
        mSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence pCharSequence, int pI, int pI1, int pI2) {

            }

            @Override
            public void onTextChanged(CharSequence pCharSequence, int pI, int pI1, int pI2) {

            }

            @Override
            public void afterTextChanged(Editable pEditable) {
                loadData(pEditable.toString());
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mReportsPresenter.dropView();
    }

    private void setData(ArrayList<Report> pReportArrayList) {
        setRefreshing(true);
        mReportsAdapter.setData(pReportArrayList);
        mReportsAdapter.notifyDataSetChanged();
        mNoDataTextView.setVisibility(View.GONE);
    }

    @UiThread
    @Override
    public void initData(ArrayList<Report> pReportArrayList) {
        Objects.requireNonNull(getActivity()).runOnUiThread(() ->
                this.setData(pReportArrayList));
    }

    @UiThread
    @Override
    public void showError() {
        Objects.requireNonNull(getActivity()).runOnUiThread(this::showNoData);
    }

    private void loadData(String pFilter) {
        mReportsPresenter.loadReports(pFilter);
    }

    void setRefreshing(boolean pVisible) {
        mProgressBar.setVisibility(pVisible ? View.GONE : View.VISIBLE);
        mRecyclerView.setVisibility(pVisible ? View.VISIBLE : View.GONE);
        mChooseAllCheckBox.setVisibility(pVisible ? View.VISIBLE : View.GONE);
    }

    void showNoData() {
        mNoDataTextView.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.GONE);
    }

    @OnClick({R.id.delete_image_view, R.id.format_archive_image_view, R.id.search_image_view})
    void onBottomAppBarClick(View pView) {
        switch (pView.getId()) {
            case R.id.delete_image_view: {
                closeDialog();

                mDialog = CustomDialog.newInstance(CustomDialog.DIALOG_DELETE_REPORT, null, new CustomDialog.OnYesNoDialogClickListener() {
                    @Override
                    public void onYesClick() {
                        setRefreshing(false);
                        mReportsPresenter.removeReports(mReportsAdapter.getSelectedItems());
                        closeDialog();
                    }

                    @Override
                    public void onNoClick() {
                        closeDialog();
                    }
                });

                mDialog.show(getFragmentManager(), CustomDialog.TAG);
            }
            break;
            case R.id.format_archive_image_view: {
                setRefreshing(false);
                mReportsPresenter.formatReport(mReportsAdapter.getSelectedItems());
            }
            break;
            case R.id.search_image_view: {
                changeSearchVisibility();
            }
            break;
        }
    }

    private void changeSearchVisibility() {
        if (!mSearchEditText.getText().toString().isEmpty()) {
            mSearchEditText.setText("");
        }
        mSearchView.setVisibility(mSearchView.getVisibility() == View.VISIBLE
                ? View.GONE : View.VISIBLE);
    }

    void closeDialog() {
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
    }

    @Override
    public void onReportClick(Report pReport) {
        mMainFragmentListener.onReportClick(pReport);
    }

    @Override
    public void onCheckedChanged(CompoundButton pCompoundButton, boolean pB) {
        mReportsAdapter.chooseAllToday(pB);
    }
}
