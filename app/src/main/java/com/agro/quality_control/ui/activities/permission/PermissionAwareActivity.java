package com.agro.quality_control.ui.activities.permission;

import android.content.Intent;

import com.agro.quality_control.R;
import com.agro.quality_control.utils.PermissionUtil;

import dagger.android.support.DaggerAppCompatActivity;

public abstract class PermissionAwareActivity extends DaggerAppCompatActivity {
    @Override
    protected void onStart() {
        super.onStart();

        boolean allPermissionsGranted = PermissionUtil.isAllNecessaryPermissionsGranted(this);
        if (!allPermissionsGranted) {
            startActivity(new Intent(this, PermissionRequestActivity.class));
            overridePendingTransition(0, R.anim.no_fade);
        }
    }
}
