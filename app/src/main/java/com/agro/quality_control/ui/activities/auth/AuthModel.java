package com.agro.quality_control.ui.activities.auth;

import android.support.annotation.NonNull;

import com.agro.quality_control.di.scopes.AuthScope;
import com.agro.quality_control.utils.ConstUtils;
import com.agro.quality_control.utils.Prefs;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import javax.inject.Inject;

@AuthScope
public class AuthModel implements AuthContract.AuthModel, FirebaseAuth.AuthStateListener {

    private FirebaseAuth mFirebaseAuth;
    private OnAuthStateListener mOnAuthListener;
    private FirebaseUser mUser;
    private Prefs mPrefs;

    @Inject
    AuthModel(FirebaseAuth pFirebaseAuth, Prefs pPrefs) {
        mFirebaseAuth = pFirebaseAuth;
        mPrefs = pPrefs;
    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth pFirebaseAuth) {
        mUser = mFirebaseAuth.getCurrentUser();
        this.checkUser();
    }

    @Override
    public void removeAuthStateListener() {
        mOnAuthListener = null;
        mFirebaseAuth.removeAuthStateListener(this);
    }

    @Override
    public void addAuthStateListener(OnAuthStateListener pOnAuthListener) {
        mOnAuthListener = pOnAuthListener;
        mFirebaseAuth.addAuthStateListener(this);
        this.checkUser();
    }

    @Override
    public void signInWithEmailAndPassword(String pEmail, String pPassword) {
        mPrefs.setAdmin(pEmail.contains(ConstUtils.ADMIN));
        mFirebaseAuth.signInWithEmailAndPassword(
                pEmail.trim(),
                pPassword.trim())
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        if (mPrefs.isAdmin()) {
                            mOnAuthListener.onSuccessAdmin();
                        } else {
                            mOnAuthListener.onSuccess();
                        }
                    } else {
                        mOnAuthListener.onFailed(task.getException());
                    }
                });
    }

    /**
     * Should be called in 2 places.
     * Sometimes onAuthStateChanged is called
     * when mOnAuthListener is not assigned.
     */
    private void checkUser() {
        if (mUser != null && mOnAuthListener != null) {
            if (mPrefs.isAdmin()) {
                mOnAuthListener.onSuccessAdmin();
            } else {
                mOnAuthListener.onSuccess();
            }
        }
    }
}
