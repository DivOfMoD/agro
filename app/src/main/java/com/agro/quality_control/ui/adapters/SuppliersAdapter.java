package com.agro.quality_control.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.agro.quality_control.R;
import com.agro.quality_control.data.models.Supplier;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SuppliersAdapter extends RecyclerView.Adapter<SuppliersAdapter.SupplierViewHolder> {

    public interface SuppliersAdapterClickListener {
        void onDeleteClick(String pId);

        void onEditClick(String pId, String pName);
    }

    private Context mContext;
    private ArrayList<Supplier> mSuppliersArrayList;

    private SuppliersAdapterClickListener mSuppliersAdapterClickListener;

    public SuppliersAdapter(Context pContext) {
        mContext = pContext;
    }

    @NonNull
    @Override
    public SupplierViewHolder onCreateViewHolder(@NonNull ViewGroup container, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(container.getContext());
        View root = inflater.inflate(R.layout.item_supplier, container, false);
        return new SupplierViewHolder(mContext, root);
    }

    @Override
    public void onBindViewHolder(@NonNull SupplierViewHolder pViewHolder, int position) {
        Supplier item = mSuppliersArrayList.get(position);
        pViewHolder.setSupplier(item.getName());
    }

    @Override
    public int getItemCount() {
        return mSuppliersArrayList != null ? mSuppliersArrayList.size() : 0;
    }

    public void setAdapterClickListener(SuppliersAdapterClickListener pSuppliersAdapterClickListener) {
        mSuppliersAdapterClickListener = pSuppliersAdapterClickListener;
    }

    public void setData(ArrayList<Supplier> pSuppliersArrayList) {
        mSuppliersArrayList = pSuppliersArrayList;
        notifyDataSetChanged();
    }

    class SupplierViewHolder extends RecyclerView.ViewHolder {

        private Context mContext;

        @BindView(R.id.supplier_container)
        ConstraintLayout mSupplierContainer;
        @BindView(R.id.supplier_text_view)
        TextView mSupplierTextView;

        SupplierViewHolder(Context pContext, @NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setSupplier(String pSupplier) {
            mSupplierTextView.setText(pSupplier);
        }

        @OnClick(R.id.delete_image_view)
        void onDeleteClick() {
            int position = getAdapterPosition();
            mSuppliersAdapterClickListener.onDeleteClick(mSuppliersArrayList.get(position).getId());
        }

        @OnClick(R.id.edit_image_view)
        void onEditClick() {
            int position = getAdapterPosition();
            mSuppliersAdapterClickListener.onEditClick(mSuppliersArrayList.get(position).getId(), mSuppliersArrayList.get(position).getName());
        }
    }

}