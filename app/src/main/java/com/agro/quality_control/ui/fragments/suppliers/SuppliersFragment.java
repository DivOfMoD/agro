package com.agro.quality_control.ui.fragments.suppliers;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.agro.quality_control.R;
import com.agro.quality_control.data.models.Supplier;
import com.agro.quality_control.ui.adapters.SuppliersAdapter;
import com.agro.quality_control.ui.base.BaseFragment;
import com.agro.quality_control.ui.views.CustomDialog;
import com.agro.quality_control.ui.views.InsetDecoration;

import java.util.ArrayList;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SuppliersFragment extends BaseFragment implements SuppliersContract.SuppliersView, SuppliersAdapter.SuppliersAdapterClickListener {

    @BindView(R.id.no_data_text_view)
    TextView mNoDataTextView;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.suppliers_recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.add_supplier_fab)
    FloatingActionButton mAddSupplierButton;

    @Inject
    SuppliersContract.SuppliersPresenter mSuppliersPresenter;
    @Inject
    InsetDecoration mInsetDecoration;
    @Inject
    LinearLayoutManager mLinearLayoutManager;
    @Inject
    SuppliersAdapter mSuppliersAdapter;

    private CustomDialog mDialog;

    public static SuppliersFragment newInstance() {
        return new SuppliersFragment();
    }

    public SuppliersFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_suppliers, container, false);
        ButterKnife.bind(this, view);
        mSuppliersPresenter.takeView(this);
        loadData();
        return view;
    }

    @Override
    public void initViews() {
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.addItemDecoration(mInsetDecoration);
        mRecyclerView.setAdapter(mSuppliersAdapter);
        mSuppliersAdapter.setAdapterClickListener(this);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mSuppliersPresenter.dropView();
    }

    private void setData(ArrayList<Supplier> pSupplierArrayList) {
        setRefreshing(true);
        mSuppliersAdapter.setData(pSupplierArrayList);
        mSuppliersAdapter.notifyDataSetChanged();
    }

    @UiThread
    @Override
    public void initData(ArrayList<Supplier> pSupplierArrayList) {
        Objects.requireNonNull(getActivity()).runOnUiThread(() ->
                this.setData(pSupplierArrayList));
    }

    @UiThread
    @Override
    public void showError() {
        Objects.requireNonNull(getActivity()).runOnUiThread(this::showNoData);
    }


    @OnClick(R.id.add_supplier_fab)
    void openSuppliersDialog() {

        closeDialog();

        mDialog = CustomDialog.newInstance(CustomDialog.DIALOG_ADD_SUPPLIER, new CustomDialog.OnAddSupplierDialogClickListener() {
            @Override
            public void onAddClick(String pName) {
                mSuppliersPresenter.addSupplier(pName);
                closeDialog();
            }

            @Override
            public void onBackClick() {
                closeDialog();
            }
        });

        mDialog.show(getFragmentManager(), CustomDialog.TAG);
    }

    private void loadData() {
        setRefreshing(false);
        mSuppliersPresenter.loadSuppliers();
    }

    void setRefreshing(boolean pShow) {
        mProgressBar.setVisibility(!pShow ? View.VISIBLE : View.GONE);
        mRecyclerView.setVisibility(pShow ? View.VISIBLE : View.GONE);
    }

    void showNoData() {
        mNoDataTextView.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.GONE);
    }

    void closeDialog() {
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
    }

    @Override
    public void onDeleteClick(String pId) {
        setRefreshing(false);
        mSuppliersPresenter.removeSupplier(pId);
    }

    @Override
    public void onEditClick(String pId, String pName) {
        closeDialog();

        mDialog = CustomDialog.newInstance(CustomDialog.DIALOG_EDIT_SUPPLIER, pId, pName, new CustomDialog.OnEditSupplierDialogClickListener() {

            @Override
            public void onEditClick(String pId, String pName) {
                mSuppliersPresenter.editSupplier(pId, pName);
                closeDialog();
            }

            @Override
            public void onBackClick() {
                closeDialog();
            }
        });

        mDialog.show(getFragmentManager(), CustomDialog.TAG);
    }
}
