package com.agro.quality_control.ui.views;

import android.content.Context;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

public abstract class ToastUtils {

    public static void showCenter(Context pContext, String pText) {
        Toast toast = Toast.makeText(pContext, pText, Toast.LENGTH_SHORT);
        TextView v = toast.getView().findViewById(android.R.id.message);
        if (v != null) v.setGravity(Gravity.CENTER);
        toast.show();
    }

    public static void showStart(Context pContext, String pText) {
        Toast toast = Toast.makeText(pContext, pText, Toast.LENGTH_SHORT);
        toast.show();
    }
}
