package com.agro.quality_control.ui.fragments.createreport;

import android.content.res.Resources;

import com.agro.quality_control.R;
import com.agro.quality_control.data.ThreadExecutors;
import com.agro.quality_control.data.api.DataType;
import com.agro.quality_control.data.api.FirebaseRepository;
import com.agro.quality_control.data.api.OnRequestListener;
import com.agro.quality_control.data.db.AgroDatabase;
import com.agro.quality_control.data.models.LocalDropdown;
import com.agro.quality_control.data.models.Report;
import com.agro.quality_control.data.models.Supplier;

import java.util.ArrayList;
import java.util.Arrays;

import javax.inject.Inject;

public class CreateReportModel implements CreateReportContract.CreateReportModel {

    private Resources mResources;
    private AgroDatabase mAgroDatabase;
    private ThreadExecutors mThreadExecutors;
    private FirebaseRepository mFirebaseRepository;
    private CreateReportContract.CreateReportPresenter.CreateReportPresenterListener mCreateReportPresenterListener;

    @Inject
    CreateReportModel(FirebaseRepository pFirebaseRepository, Resources pResources, AgroDatabase pAgroDatabase, ThreadExecutors pThreadExecutors) {
        mFirebaseRepository = pFirebaseRepository;
        mResources = pResources;
        mAgroDatabase = pAgroDatabase;
        mThreadExecutors = pThreadExecutors;
    }

    @Override
    public void removeRequestListener() {
        mCreateReportPresenterListener = null;
    }

    @Override
    public void addRequestListener(CreateReportContract.CreateReportPresenter.CreateReportPresenterListener pListener) {
        mCreateReportPresenterListener = pListener;
    }

    @Override
    public void createReport(Report pReport) {
        mThreadExecutors.dbExecutor().execute(() -> {
            mAgroDatabase.getReportDao().insertReportData(pReport);
            mCreateReportPresenterListener.onReportCreated();
        });
    }

    @Override
    public void updateReport(Report pReport) {
        mThreadExecutors.dbExecutor().execute(() -> {
            mAgroDatabase.getReportDao().update(pReport);
            mCreateReportPresenterListener.onReportUpdated();
        });
    }

    @Override
    public void getDropdownLocalData() {
        mCreateReportPresenterListener.onAPIDataLoaded(new LocalDropdown(
                new ArrayList<>(Arrays.asList(mResources.getStringArray(R.array.cultures_array))),
                new ArrayList<>(Arrays.asList(mResources.getStringArray(R.array.culture_year_array))),
                new ArrayList<>(Arrays.asList(mResources.getStringArray(R.array.garbage_array))),
                new ArrayList<>(Arrays.asList(mResources.getStringArray(R.array.smell_array))),
                new ArrayList<>(Arrays.asList(mResources.getStringArray(R.array.unloading_array)))
        ));
    }

    @Override
    public void getDropdownServerData() {
        mFirebaseRepository.getSuppliers(new OnRequestListener<Supplier>() {
            @Override
            public void onSucceed(ArrayList<Supplier> pObject) {
                ArrayList<String> result = new ArrayList<>();

                for (Supplier supplier : pObject) {
                    result.add(supplier.getName());
                }
                mCreateReportPresenterListener.onAPIDataLoaded(result);
            }

            @Override
            public void onSucceed(Supplier pObject) {

            }

            @Override
            public void onSucceed(DataType pDataType) {
            //TODO NOT CALLED
            }

            @Override
            public void onFailed(DataType pDataType, String pMessage) {

            }
        });
    }

    @Override
    public void getUnloading2Data(int pPosition) {
        switch (pPosition) {
            case 1: {
                mCreateReportPresenterListener.onUnloading2DataLoaded
                        (new ArrayList<>(Arrays.asList(mResources.getStringArray(R.array.shop_array))));
            }
            break;
            case 5: {
                mCreateReportPresenterListener.onUnloading2DataLoaded
                        (new ArrayList<>(Arrays.asList(mResources.getStringArray(R.array.barrel1_array))));
            }
            break;
            case 6: {
                mCreateReportPresenterListener.onUnloading2DataLoaded
                        (new ArrayList<>(Arrays.asList(mResources.getStringArray(R.array.barrel2_array))));
            }
            break;
        }
    }
}
