package com.agro.quality_control.ui.fragments.archive;

import android.support.annotation.Nullable;

import com.agro.quality_control.data.models.ReportFile;

import java.util.ArrayList;

import javax.inject.Inject;

public class ArchivePresenter implements ArchiveContract.ArchivePresenter, ArchiveContract.ArchivePresenter.ArchivePresenterListener {

    @Nullable
    private ArchiveContract.ArchiveView mArchiveView;
    private ArchiveModel mArchiveModel;

    @Inject
    public ArchivePresenter(ArchiveModel pArchiveModel) {
        mArchiveModel = pArchiveModel;
    }

    @Override
    public void takeView(ArchiveContract.ArchiveView view) {
        mArchiveView = view;
        mArchiveModel.addRequestListener(this);
        if (mArchiveView != null) {
            mArchiveView.initViews();
        }
    }

    @Override
    public void dropView() {
        mArchiveView = null;
        mArchiveModel.removeRequestListener();
    }


    @Override
    public void loadReportFiles() {
        mArchiveModel.getStorageFiles();
    }

    @Override
    public void removeStorageFile(ReportFile pReportFile) {
        mArchiveModel.deleteStorageFile(pReportFile);
    }

    @Override
    public void downloadStorageFile(ReportFile pReportFile) {
        mArchiveModel.getStorageFile(pReportFile);
    }

    @Override
    public void shareStorageFile(ReportFile pReportFile) {

    }

    @Override
    public void openStorageFile(ReportFile pReportFile) {

    }

    @Override
    public void onFileReportsLoaded(ArrayList<ReportFile> pReportArrayList) {
        if (mArchiveView != null) {
            mArchiveView.initData(pReportArrayList);
        }
    }

    @Override
    public void onDownloaded() {
        if (mArchiveView != null) {
            mArchiveView.showDownloadSuccess();
        }
    }

    @Override
    public void onShare() {

    }

    @Override
    public void onOpen() {

    }

    @Override
    public void onFailed() {
        if (mArchiveView != null) {
            mArchiveView.initData(new ArrayList<>());
        }
    }
}
