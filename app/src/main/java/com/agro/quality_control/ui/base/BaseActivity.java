package com.agro.quality_control.ui.base;

import com.agro.quality_control.R;
import com.agro.quality_control.ui.activities.permission.PermissionAwareActivity;


public abstract class BaseActivity extends PermissionAwareActivity {

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}