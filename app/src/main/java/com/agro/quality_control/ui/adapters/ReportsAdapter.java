package com.agro.quality_control.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.agro.quality_control.R;
import com.agro.quality_control.data.db.AgroDatabase;
import com.agro.quality_control.data.models.HeaderDate;
import com.agro.quality_control.data.models.RecyclerItem;
import com.agro.quality_control.data.models.Report;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ReportsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public interface ReportsAdapterClickListener {
        void onReportClick(Report pReport);
    }

    private Context mContext;
    private ArrayList<RecyclerItem> mRecyclerArrayList;

    private ReportsAdapterClickListener mReportsAdapterClickListener;

    public ReportsAdapter(Context pContext) {
        mContext = pContext;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup container, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(container.getContext());
        if (viewType == RecyclerItem.TYPE_HEADER) {
            View root = inflater.inflate(R.layout.item_date, container, false);
            return new DatesViewHolder(mContext, root);
        } else {
            View root = inflater.inflate(R.layout.item_report, container, false);
            return new ReportsViewHolder(mContext, root);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder pViewHolder, int position) {
        if (pViewHolder instanceof ReportsViewHolder) {
            ReportsViewHolder viewHolder = (ReportsViewHolder) pViewHolder;
            Report item = (Report) mRecyclerArrayList.get(position);

            viewHolder.setSyncStat(item.getSyncState());
            viewHolder.setPosition(position);
            viewHolder.setDate(item.getTimestamp());
            viewHolder.setChecked(item.isChecked());
            viewHolder.setText(item.getBoxNumber(), item.getDocNumber());
        } else {
            DatesViewHolder viewHolder = (DatesViewHolder) pViewHolder;
            HeaderDate item = (HeaderDate) mRecyclerArrayList.get(position);

            viewHolder.setDate(item.getDate());
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mRecyclerArrayList.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return mRecyclerArrayList != null ? mRecyclerArrayList.size() : 0;
    }

    public void setData(ArrayList<Report> pReportsArrayList) {
        Collections.sort(pReportsArrayList, (lhs, rhs) ->
                (int) (rhs.getTimestamp() - lhs.getTimestamp()));
        mRecyclerArrayList = getFormattedData(pReportsArrayList);
    }

    private ArrayList<RecyclerItem> getFormattedData(ArrayList<Report> pReportsArrayList) {
        ArrayList<String> listOfDates = new ArrayList<>();
        ArrayList<RecyclerItem> result = new ArrayList<>();

        for (Report item : pReportsArrayList) {
            String date = getDate(item.getTimestamp());
            if (!listOfDates.contains(date)) {
                listOfDates.add(date);
            }
        }

        for (int i = 0; i < listOfDates.size(); i++) {
            result.add(new HeaderDate(listOfDates.get(i)));
            for (int j = 0; j < pReportsArrayList.size(); j++) {
                String date = getDate(pReportsArrayList.get(j).getTimestamp());
                if (listOfDates.get(i).equals(date)) {
                    result.add(pReportsArrayList.get(j));
                }
            }
        }
        return result;
    }

    public void sortReportsByBoxNumber() {
        Collections.sort(mRecyclerArrayList, (lhs, rhs) -> {
            if (lhs instanceof Report && rhs instanceof Report
                    && isToady(((Report) lhs).getTimestamp()) && isToady(((Report) rhs).getTimestamp())) {
                return (((Report) lhs).getBoxNumberInteger() - ((Report) rhs).getBoxNumberInteger());
            } else {
                return 0;
            }
        });
    }

    public List<Report> getSelectedItems() {
        List<Report> reportList = new ArrayList<>();

        if (mRecyclerArrayList != null) {
            for (int i = 0; i < mRecyclerArrayList.size(); i++) {
                if (mRecyclerArrayList.get(i) instanceof Report && mRecyclerArrayList.get(i).isChecked()) {
                    reportList.add((Report) mRecyclerArrayList.get(i));
                }
            }
        }
        return reportList;
    }

    public void chooseAllToday(boolean pState) {
        if (mRecyclerArrayList != null) {
            for (int i = 0; i < mRecyclerArrayList.size(); i++) {
//                if (mRecyclerArrayList.get(i) instanceof Report && isToady(((Report) mRecyclerArrayList.get(i)).getTimestamp())) {
                if (mRecyclerArrayList.get(i) instanceof Report) {
                    mRecyclerArrayList.get(i).setChecked(pState);
                }
            }
            notifyDataSetChanged();
        }
    }

    private boolean isToady(long pTimestamp) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(pTimestamp);

        Calendar calendarCurrent = Calendar.getInstance();
        calendarCurrent.setTimeInMillis(System.currentTimeMillis());

        return calendar.get(Calendar.YEAR) == calendarCurrent.get(Calendar.YEAR) &&
                calendar.get(Calendar.MONTH) == calendarCurrent.get(Calendar.MONTH) &&
                calendar.get(Calendar.DAY_OF_MONTH) == calendarCurrent.get(Calendar.DAY_OF_MONTH);
    }

    public void setReportsAdapterClickListener(ReportsAdapterClickListener
                                                       pReportsAdapterClickListener) {
        mReportsAdapterClickListener = pReportsAdapterClickListener;
    }

    class DatesViewHolder extends RecyclerView.ViewHolder {

        private Context mContext;

        @BindView(R.id.date_text_view)
        TextView mDateTextView;

        DatesViewHolder(Context pContext, @NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setDate(String pDate) {
            mDateTextView.setText(pDate);
        }
    }

    class ReportsViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {

        private Context mContext;

        @BindView(R.id.report_container)
        ConstraintLayout mContainer;
        @BindView(R.id.date_text_view)
        TextView mDateTextView;
        @BindView(R.id.report_text_view)
        TextView mReportTextView;
        @BindView(R.id.report_check_box)
        CheckBox mCheckBox;

        ReportsViewHolder(Context pContext, View pItemView) {
            super(pItemView);
            ButterKnife.bind(this, pItemView);
            mContext = pContext;
            mContainer.setOnClickListener(this::onClick);
            mCheckBox.setOnCheckedChangeListener(this);
        }

        public void setDate(long pTimestamp) {
            mDateTextView.setText(getFormattedDate(pTimestamp));
        }

        public void setChecked(boolean pChecked) {
            mCheckBox.setChecked(pChecked);
        }

        void setText(String pBoxNumber, String pDocNumber) {
            mReportTextView.setText(mContext.getString(R.string.report_text, pBoxNumber, pDocNumber));
        }

        void setPosition(int pPosition) {
            mContainer.setTag(pPosition);
        }

        void setSyncStat(int pSyncState) {
            if (pSyncState == AgroDatabase.SYNC_DRAFT) {
                mContainer.setForeground(mContext.getResources().getDrawable(R.drawable.bg_rounded_red_button,
                        Objects.requireNonNull(mContext).getTheme()));
            }
        }

        void onClick(View v) {
            Report report = (Report) mRecyclerArrayList.get((Integer) v.getTag());
            mReportsAdapterClickListener.onReportClick(report);
        }

        @Override
        public void onCheckedChanged(CompoundButton pCompoundButton, boolean pB) {
            mRecyclerArrayList.get((Integer) mContainer.getTag()).setChecked(pB);
        }
    }

    private String getDate(long pTimestamp) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(pTimestamp);

        Calendar result = Calendar.getInstance();
        result.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
        result.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
        result.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH));
        return getFormattedDate(result);
    }

    private String getFormattedDate(long pTimestamp) {
        SimpleDateFormat formatter = new SimpleDateFormat("hh:mm - dd.MM.yyyy", Locale.getDefault());
        return formatter.format(new Date(pTimestamp));
    }

    private String getFormattedDate(Calendar pCalendar) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        return formatter.format(pCalendar.getTime());
    }
}