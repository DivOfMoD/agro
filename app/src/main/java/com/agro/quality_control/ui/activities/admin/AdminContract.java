package com.agro.quality_control.ui.activities.admin;

import com.agro.quality_control.ui.base.BasePresenter;

public interface AdminContract {

    interface AdminView {
        void initViews();

        void navigateAuthPage();

        void showSignOutError(String pE);
    }

    interface AdminPresenter extends BasePresenter<AdminView> {
        void signOut();
    }

    interface AdminModel {
        void signOut();

        interface OnSignOutStateListener {
            void onSuccess();

            void onFailed(Exception pE);
        }

        void removeSignOutStateListener();

        void addSignOutStateListener(OnSignOutStateListener pOnSignOutStateListener);

    }

    interface FragmentListener {
    }

}
