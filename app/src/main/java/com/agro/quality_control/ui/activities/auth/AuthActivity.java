package com.agro.quality_control.ui.activities.auth;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.agro.quality_control.R;
import com.agro.quality_control.ui.NavigationUtils;
import com.agro.quality_control.ui.activities.admin.AdminActivity;
import com.agro.quality_control.ui.activities.main.MainActivity;
import com.agro.quality_control.ui.base.BaseActivity;
import com.agro.quality_control.ui.views.AuthEditText;
import com.agro.quality_control.ui.views.ToastUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AuthActivity extends BaseActivity implements AuthContract.AuthView {

    @BindView(R.id.login_container)
    ConstraintLayout mLoginContainer;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.login_auth_edit_text)
    AuthEditText mLoginAuthEditText;
    @BindView(R.id.password_auth_edit_text)
    AuthEditText mPasswordAuthEditText;

    @Inject
    AuthContract.AuthPresenter mAuthPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        ButterKnife.bind(this);
        mAuthPresenter.takeView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAuthPresenter.dropView();
    }

    @Override
    public void navigateMainScreen() {
        NavigationUtils.routeToAppropriatePage(this, MainActivity.class);
        finish();
    }

    @Override
    public void navigateAdminScreen() {
        NavigationUtils.routeToAppropriatePage(this, AdminActivity.class);
        finish();
    }

    @Override
    public void showAuthFailed(AuthContract.AuthEnum pAuthEnum, @Nullable Exception pE) {
        changeVisibility(true);
        switch (pAuthEnum) {
            case EMAIL: {
                setEmailError();
            }
            break;
            case PASSWORD: {
                setPasswordError();
            }
            break;
            case FIREBASE: {
                if (pE != null) {
                    String message = pE.getMessage();
                    ToastUtils.showStart(this, message);
                }
            }
            break;
        }
    }


    @OnClick(R.id.sign_in_text_view)
    void signIn() {
        changeVisibility(false);
        mAuthPresenter.signIn(mLoginAuthEditText.getText(), mPasswordAuthEditText.getText());
    }

    void changeVisibility(boolean isVisible) {
        mLoginContainer.setVisibility(isVisible ? View.VISIBLE : View.INVISIBLE);
        mProgressBar.setVisibility(!isVisible ? View.VISIBLE : View.INVISIBLE);
    }

    void setEmailError() {
        ToastUtils.showCenter(AuthActivity.this, getString(R.string.error_login));
        requestFocus(mLoginAuthEditText);
    }

    void setPasswordError() {
        ToastUtils.showCenter(AuthActivity.this, getString(R.string.error_password));
        requestFocus(mPasswordAuthEditText);
    }

    void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

}
