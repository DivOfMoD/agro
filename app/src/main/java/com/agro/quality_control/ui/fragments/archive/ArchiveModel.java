package com.agro.quality_control.ui.fragments.archive;

import com.agro.quality_control.data.api.DataType;
import com.agro.quality_control.data.api.FirebaseRepository;
import com.agro.quality_control.data.api.OnRequestListener;
import com.agro.quality_control.data.models.ReportFile;
import com.agro.quality_control.utils.FileUtils;

import java.io.File;
import java.util.ArrayList;

import javax.inject.Inject;

public class ArchiveModel implements ArchiveContract.ArchiveModel {

    private FirebaseRepository mFirebaseRepository;
    private ArchiveContract.ArchivePresenter.ArchivePresenterListener mListener;

    @Inject
    ArchiveModel(FirebaseRepository pFirebaseRepository) {
        mFirebaseRepository = pFirebaseRepository;
    }

    @Override
    public void removeRequestListener() {
        mListener = null;
    }

    @Override
    public void addRequestListener(ArchiveContract.ArchivePresenter.ArchivePresenterListener pListener) {
        mListener = pListener;
    }

    @Override
    public void getStorageFiles() {
        mFirebaseRepository.getReportFileInfo(new OnRequestListener<ReportFile>() {
            @Override
            public void onSucceed(ArrayList<ReportFile> pObject) {
                mListener.onFileReportsLoaded(pObject);
            }

            @Override
            public void onSucceed(ReportFile pObject) {
                //TODO NOT CALLED
            }

            @Override
            public void onSucceed(DataType pDataType) {
                //TODO NOT CALLED
            }

            @Override
            public void onFailed(DataType pDataType, String pMessage) {
                mListener.onFailed();
            }
        });
    }

    @Override
    public void deleteStorageFile(ReportFile pReportFile) {
        mFirebaseRepository.deleteXLS(pReportFile, new OnRequestListener() {
            @Override
            public void onSucceed(ArrayList pObject) {
                //TODO NOT CALLED
            }

            @Override
            public void onSucceed(Object pObject) {
                //TODO NOT CALLED
            }

            @Override
            public void onSucceed(DataType pDataType) {
                FileUtils.deleteFile(pReportFile.getName());
//                getStorageFiles(); //TODO NO NEED TO HANDLE, JUST RELOAD
            }

            @Override
            public void onFailed(DataType pDataType, String pMessage) {
                mListener.onFailed();
            }
        });
    }

    @Override
    public void getStorageFile(ReportFile pReportFile) {
        File file = FileUtils.getLocalTempFile(pReportFile.getName());
        mFirebaseRepository.downloadXLS(file, new OnRequestListener() {
            @Override
            public void onSucceed(ArrayList pObject) {

            }

            @Override
            public void onSucceed(Object pObject) {

            }

            @Override
            public void onSucceed(DataType pDataType) {
                mListener.onDownloaded();
            }

            @Override
            public void onFailed(DataType pDataType, String pMessage) {

            }
        });
    }

    @Override
    public void shareStorageFile(ReportFile pReportFile) {

    }

    @Override
    public void openStorageFile(ReportFile pReportFile) {

    }
}
