package com.agro.quality_control.ui.activities.auth;

import android.support.annotation.Nullable;

import com.agro.quality_control.ui.base.BasePresenter;


public interface AuthContract {

    enum AuthEnum {
        EMAIL,
        PASSWORD,
        FIREBASE
    }

    interface AuthView {

        void navigateMainScreen();

        void navigateAdminScreen();

        void showAuthFailed(AuthEnum pAuthEnum, @Nullable Exception pException);
    }

    interface AuthPresenter extends BasePresenter<AuthView> {
        void signIn(String pEmail, String pPassword);
    }

    interface AuthModel {

        interface OnAuthStateListener {
            void onSuccess();

            void onSuccessAdmin();

            void onFailed(Exception pE);
        }

        void removeAuthStateListener();

        void addAuthStateListener(OnAuthStateListener pOnAuthListener);

        void signInWithEmailAndPassword(String pEmail, String pPassword);
    }
}
