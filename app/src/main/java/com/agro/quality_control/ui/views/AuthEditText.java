package com.agro.quality_control.ui.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatImageView;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.agro.quality_control.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AuthEditText extends LinearLayout {


    @BindView(R.id.icon_image_view)
    AppCompatImageView mIconImageView;
    @BindView(R.id.auth_edit_text)
    EditText mAuthEditText;

    public AuthEditText(Context context) {
        super(context);
        initAttr(context, null);
    }

    public AuthEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttr(context, attrs);
    }

    public AuthEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttr(context, attrs);
    }

    private void initAttr(Context pContext, AttributeSet pAttributeSet) {
        int pattern;
        String hint;
        Drawable drawable;

        TypedArray ta = pContext.obtainStyledAttributes(pAttributeSet, R.styleable.AuthEditText);
        try {
            pattern = ta.getInt(R.styleable.AuthEditText_text_pattern, 0);
            hint = ta.getString(R.styleable.AuthEditText_hint_text);
            drawable = ta.getDrawable(R.styleable.AuthEditText_icon_src);
        } finally {
            ta.recycle();
        }

        init(pContext, pattern, hint, drawable);
    }

    public void init(Context pContext, int pPattern, String pHint, Drawable pDrawable) {
        LayoutInflater.from(pContext).inflate(R.layout.view_auth_edit_text, this, true);
        ButterKnife.bind(this);

        mAuthEditText.setHint(pHint);
        if (pPattern == InputType.TYPE_TEXT_VARIATION_PASSWORD) {
            mAuthEditText.setInputType(InputType.TYPE_CLASS_TEXT |
                    InputType.TYPE_TEXT_VARIATION_PASSWORD);
        } else {
            mAuthEditText.setInputType(pPattern);
        }

        mIconImageView.setBackground(pDrawable);
    }

    public String getText() {
        return mAuthEditText.getText().toString();
    }
}
