package com.agro.quality_control.ui.fragments.suppliers;

import com.agro.quality_control.data.models.Supplier;
import com.agro.quality_control.ui.base.BaseModel;
import com.agro.quality_control.ui.base.BasePresenter;

import java.util.ArrayList;


public interface SuppliersContract {

    interface SuppliersView {

        void showError();

        void initData(ArrayList<Supplier> pReportArrayList);

        void initViews();
    }

    interface SuppliersPresenter extends BasePresenter<SuppliersView> {

        interface SuppliersPresenterListener {
            void onSuppliersLoaded(ArrayList<Supplier> pReportArrayList);

            void onSupplierDeleted();

            void onSupplierAdded();

            void onSupplierEdited();

            void onFailed();
        }

        void loadSuppliers();

        void addSupplier(String pName);

        void editSupplier(String pId, String pName);

        void removeSupplier(String pId);
    }

    interface SuppliersModel extends BaseModel<SuppliersPresenter.SuppliersPresenterListener> {

        void getSuppliers();

        void setSupplier(String pName);

        void updateSupplier(String pId, String pName);

        void deleteSupplier(String pId);
    }

}
