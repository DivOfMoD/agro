package com.agro.quality_control.ui.views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.agro.quality_control.R;
import com.agro.quality_control.ui.adapters.SearchSuppliersAdapter;
import com.agro.quality_control.ui.fragments.createreport.CreateReportContract;

import java.util.ArrayList;

public class CustomDialog extends DialogFragment {

    private static final String EXTRA_DIALOG_TYPE = "EXTRA_DIALOG_TYPE";
    private static final String EXTRA_SUPPLIERS = "EXTRA_SUPPLIERS";
    private static final String EXTRA_REPORT_ENUM = "EXTRA_REPORT_ENUM";
    private static final String EXTRA_SUPPLIER_ID = "EXTRA_SUPPLIER_ID";
    private static final String EXTRA_SUPPLIER_NAME = "EXTRA_SUPPLIER_NAME";
    public static final String TAG = "CustomDialog";

    public static final int DIALOG_SAVE_REPORT_CORE = 0;
    public static final int DIALOG_SAVE_REPORT_DRAFT = 1;
    public static final int DIALOG_DELETE_REPORT = 2;
    public static final int DIALOG_SUPPLIER = 3;
    public static final int DIALOG_ADD_SUPPLIER = 4;
    public static final int DIALOG_EDIT_SUPPLIER = 5;

    public interface OnSupplierDialogClickListener {
        void onCloseClick();

        void onSupplierClick(String pSupplier);
    }

    public interface OnYesNoDialogClickListener {
        void onYesClick();

        void onNoClick();
    }

    public interface OnAddSupplierDialogClickListener {
        void onAddClick(String pName);

        void onBackClick();
    }


    public interface OnEditSupplierDialogClickListener {
        void onEditClick(String pId, String pName);

        void onBackClick();
    }

    static OnSupplierDialogClickListener sSupplierDialogClickListener;
    static OnYesNoDialogClickListener sYesNoDialogClickListener;
    static OnAddSupplierDialogClickListener sAddSupplierDialogClickListener;
    static OnEditSupplierDialogClickListener sEditSupplierDialogClickListener;


    public static CustomDialog newInstance(int type, ArrayList<String> pSupplierList, OnSupplierDialogClickListener dialogClickListener) {
        CustomDialog f = new CustomDialog();
        sSupplierDialogClickListener = dialogClickListener;
        Bundle args = new Bundle();
        args.putInt(EXTRA_DIALOG_TYPE, type);
        args.putStringArrayList(EXTRA_SUPPLIERS, pSupplierList);

        f.setArguments(args);
        return f;
    }

    public static CustomDialog newInstance(int type, @Nullable ArrayList<CreateReportContract.CreateReportEnum> pCreateReportEnum, OnYesNoDialogClickListener dialogClickListener) {
        CustomDialog f = new CustomDialog();
        sYesNoDialogClickListener = dialogClickListener;
        Bundle args = new Bundle();
        args.putInt(EXTRA_DIALOG_TYPE, type);
        args.putParcelableArrayList(EXTRA_REPORT_ENUM, pCreateReportEnum);

        f.setArguments(args);
        return f;
    }

    public static CustomDialog newInstance(int type, OnAddSupplierDialogClickListener dialogClickListener) {
        CustomDialog f = new CustomDialog();
        sAddSupplierDialogClickListener = dialogClickListener;
        Bundle args = new Bundle();
        args.putInt(EXTRA_DIALOG_TYPE, type);

        f.setArguments(args);
        return f;
    }

    public static CustomDialog newInstance(int type, String pId, String pName, OnEditSupplierDialogClickListener dialogClickListener) {
        CustomDialog f = new CustomDialog();
        sEditSupplierDialogClickListener = dialogClickListener;
        Bundle args = new Bundle();
        args.putInt(EXTRA_DIALOG_TYPE, type);
        args.putString(EXTRA_SUPPLIER_ID, pId);
        args.putString(EXTRA_SUPPLIER_NAME, pName);

        f.setArguments(args);
        return f;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        int type;
        View view = null;
        Context context = getContext();

        if (getArguments() != null) {
            type = getArguments().getInt(EXTRA_DIALOG_TYPE);
        } else {
            return null;
        }

        switch (type) {
            case DIALOG_SAVE_REPORT_CORE: {

                view = inflater.inflate(R.layout.view_dialog_report, container, false);
                if (context != null && getDialog() != null && getDialog().getWindow() != null) {
                    getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    setCancelable(true);

                    ArrayList<CreateReportContract.CreateReportEnum> enumList =
                            getArguments().getParcelableArrayList(EXTRA_REPORT_ENUM);
                    String baseFields = null;
                    if (enumList != null) {
                        baseFields = getBaseFields(enumList);
                    }

                    TextView titleTextView = view.findViewById(R.id.dialog_title_text_view);
                    TextView descriptionTextView = view.findViewById(R.id.dialog_description_text_view);
                    TextView noTextView = view.findViewById(R.id.no_text_view);
                    TextView yesTextView = view.findViewById(R.id.yes_text_view);

                    noTextView.setText(R.string.dialog_back);
                    titleTextView.setText(R.string.dialog_title_create_report);
                    descriptionTextView.setText(getString(R.string.dialog_description_create_core_report, baseFields));

                    noTextView.setOnClickListener(view1 -> sYesNoDialogClickListener.onNoClick());
                    yesTextView.setVisibility(View.GONE);
                }
            }
            break;
            case DIALOG_SAVE_REPORT_DRAFT: {

                view = inflater.inflate(R.layout.view_dialog_report, container, false);
                if (context != null && getDialog() != null && getDialog().getWindow() != null) {
                    getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    setCancelable(true);

                    TextView titleTextView = view.findViewById(R.id.dialog_title_text_view);
                    TextView descriptionTextView = view.findViewById(R.id.dialog_description_text_view);
                    TextView noTextView = view.findViewById(R.id.no_text_view);
                    TextView yesTextView = view.findViewById(R.id.yes_text_view);

                    titleTextView.setText(R.string.dialog_title_create_report);
                    descriptionTextView.setText(R.string.dialog_description_create_draft_report);

                    noTextView.setOnClickListener(view1 -> sYesNoDialogClickListener.onNoClick());
                    yesTextView.setOnClickListener(view1 -> sYesNoDialogClickListener.onYesClick());
                }
            }
            break;
            case DIALOG_DELETE_REPORT: {

                view = inflater.inflate(R.layout.view_dialog_report, container, false);
                if (context != null && getDialog() != null && getDialog().getWindow() != null) {
                    getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    setCancelable(true);

                    TextView titleTextView = view.findViewById(R.id.dialog_title_text_view);
                    TextView descriptionTextView = view.findViewById(R.id.dialog_description_text_view);
                    TextView noTextView = view.findViewById(R.id.no_text_view);
                    TextView yesTextView = view.findViewById(R.id.yes_text_view);

                    titleTextView.setText(R.string.dialog_title_delete_report);
                    descriptionTextView.setText(R.string.dialog_description_delete_report);

                    noTextView.setOnClickListener(view1 -> sYesNoDialogClickListener.onNoClick());
                    yesTextView.setOnClickListener(view1 -> sYesNoDialogClickListener.onYesClick());
                }
            }
            break;
            case DIALOG_SUPPLIER: {

                view = inflater.inflate(R.layout.view_dialog_supplier, container, false);
                if (context != null && getDialog() != null && getDialog().getWindow() != null) {
                    getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    setCancelable(true);

                    ArrayList<String> suppliersList = getArguments().getStringArrayList(EXTRA_SUPPLIERS);

                    RecyclerView recyclerView = view.findViewById(R.id.supplier_recycler_view);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(context);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(layoutManager);
                    DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(context,
                            layoutManager.getOrientation());
                    recyclerView.addItemDecoration(dividerItemDecoration);

                    SearchSuppliersAdapter adapter = new SearchSuppliersAdapter(context);
                    adapter.setData(suppliersList);
                    recyclerView.setAdapter(adapter);
                    adapter.setAdapterClickListener(supplier -> {
                        sSupplierDialogClickListener.onSupplierClick(supplier);
                        dismiss();
                    });

                    ImageView closeImageVIew = view.findViewById(R.id.close_image_view);
                    closeImageVIew.setOnClickListener(v -> sSupplierDialogClickListener.onCloseClick());

                    EditText searchEditText = view.findViewById(R.id.search_edit_text);
                    searchEditText.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence pCharSequence, int pI, int pI1, int pI2) {

                        }

                        @Override
                        public void onTextChanged(CharSequence pCharSequence, int pI, int pI1, int pI2) {
                            String filter = pCharSequence.toString();
                            ArrayList<String> filteredSuppliersArrayList = new ArrayList<>();

                            if (suppliersList != null) {
                                for (String item : suppliersList) {
                                    if (item.toLowerCase().contains(filter.toLowerCase())) {
                                        filteredSuppliersArrayList.add(item);
                                    }
                                }
                            }
                            adapter.setData(filteredSuppliersArrayList);
                        }

                        @Override
                        public void afterTextChanged(Editable pEditable) {

                        }
                    });
                }
            }
            break;
            case DIALOG_ADD_SUPPLIER: {
                view = inflater.inflate(R.layout.view_dialog_add_supplier, container, false);
                if (context != null && getDialog() != null && getDialog().getWindow() != null) {
                    getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    setCancelable(true);

                    TextView backTextView = view.findViewById(R.id.back_text_view);
                    TextView addTextView = view.findViewById(R.id.add_text_view);
                    EditText addSupplierEditText = view.findViewById(R.id.add_supplier_edit_text);

                    backTextView.setOnClickListener(view1 -> sAddSupplierDialogClickListener.onBackClick());
                    addTextView.setOnClickListener(view1 -> {
                        if (addSupplierEditText.getText().toString().length() > 2) {
                            sAddSupplierDialogClickListener.onAddClick(addSupplierEditText.getText().toString());
                        } else {
                            ToastUtils.showCenter(context, getString(R.string.error_add_supplier));
                        }
                    });
                }
            }
            break;
            case DIALOG_EDIT_SUPPLIER: {
                view = inflater.inflate(R.layout.view_dialog_add_supplier, container, false);
                if (context != null && getDialog() != null && getDialog().getWindow() != null) {
                    getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                    setCancelable(true);

                    String id = getArguments().getString(EXTRA_SUPPLIER_ID);
                    String name = getArguments().getString(EXTRA_SUPPLIER_NAME);

                    TextView backTextView = view.findViewById(R.id.back_text_view);
                    TextView addTextView = view.findViewById(R.id.add_text_view);
                    EditText addSupplierEditText = view.findViewById(R.id.add_supplier_edit_text);

                    addSupplierEditText.setText(name);
//                    addSupplierEditText.setSelection(name.length());

                    backTextView.setOnClickListener(view1 -> sEditSupplierDialogClickListener.onBackClick());
                    addTextView.setOnClickListener(view1 -> {
                        if (addSupplierEditText.getText().toString().length() > 2) {
                            sEditSupplierDialogClickListener.onEditClick(id, addSupplierEditText.getText().toString());
                        } else {
                            ToastUtils.showCenter(context, getString(R.string.error_add_supplier));
                        }
                    });
                }
            }
            break;
        }
        return view;
    }

    private String getBaseFields(ArrayList<CreateReportContract.CreateReportEnum> pEnumList) {
        StringBuilder stringBuilder = new StringBuilder();
        for (CreateReportContract.CreateReportEnum reportEnum : pEnumList) {
            stringBuilder.append("\n");
            switch (reportEnum) {
                case BOX_NUMBER: {
                    stringBuilder.append(getString(R.string.hint_box_number));
                }
                break;
                case CULTURE: {
                    stringBuilder.append(getString(R.string.title_cultures));
                }
                break;
                case DOC_NUMBER: {
                    stringBuilder.append(getString(R.string.hint_doc_number));
                }
                break;
                case SUPPLIER: {
                    stringBuilder.append(getString(R.string.title_supplier));
                }
                break;
            }
        }
        return stringBuilder.toString();
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commit();
        } catch (IllegalStateException e) {
            Log.d(TAG, e.toString());
        }
    }

    @Override
    public void dismiss() {
        try {
            super.dismiss();
        } catch (IllegalStateException ex) {
            Log.d(TAG, ex.toString());
        }
    }
}