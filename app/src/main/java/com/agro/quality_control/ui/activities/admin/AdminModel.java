package com.agro.quality_control.ui.activities.admin;

import com.google.firebase.auth.FirebaseAuth;

import javax.inject.Inject;

public class AdminModel implements AdminContract.AdminModel {

    private FirebaseAuth mFirebaseAuth;
    private OnSignOutStateListener mOnSignOutStateListener;

    @Inject
    AdminModel(FirebaseAuth pFirebaseAuth) {
        mFirebaseAuth = pFirebaseAuth;
    }

    @Override
    public void signOut() {
        try {
            mFirebaseAuth.signOut();
            mOnSignOutStateListener.onSuccess();
        } catch (Exception ex) {
            mOnSignOutStateListener.onFailed(ex);
        }
    }

    @Override
    public void removeSignOutStateListener() {
        mOnSignOutStateListener = null;
    }

    @Override
    public void addSignOutStateListener(OnSignOutStateListener pOnSignOutStateListener) {
        mOnSignOutStateListener = pOnSignOutStateListener;
    }
}
