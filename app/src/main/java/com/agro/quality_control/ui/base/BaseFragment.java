package com.agro.quality_control.ui.base;

import com.agro.quality_control.ui.activities.main.MainActivity;
import com.agro.quality_control.ui.activities.main.MainContract;

import dagger.android.support.DaggerFragment;

public abstract class BaseFragment extends DaggerFragment {

    protected MainContract.FragmentListener mMainFragmentListener;

    public void setMainFragmentListener(MainActivity pActivity) {
        mMainFragmentListener = pActivity;
    }

}