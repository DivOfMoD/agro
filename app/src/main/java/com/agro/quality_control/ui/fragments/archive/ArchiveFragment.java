package com.agro.quality_control.ui.fragments.archive;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.agro.quality_control.R;
import com.agro.quality_control.data.models.ReportFile;
import com.agro.quality_control.ui.adapters.ArchiveAdapter;
import com.agro.quality_control.ui.base.BaseFragment;
import com.agro.quality_control.ui.views.CustomDialog;
import com.agro.quality_control.ui.views.InsetDecoration;
import com.agro.quality_control.ui.views.ToastUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ArchiveFragment extends BaseFragment implements ArchiveContract.ArchiveView, ArchiveAdapter.ArchiveAdapterClickListener {

    @BindView(R.id.no_data_text_view)
    TextView mNoDataTextView;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.archive_recycler_view)
    RecyclerView mRecyclerView;

    @Inject
    ArchiveContract.ArchivePresenter mArchivePresenter;
    @Inject
    InsetDecoration mInsetDecoration;
    @Inject
    LinearLayoutManager mLinearLayoutManager;
    @Inject
    ArchiveAdapter mArchiveAdapter;

    private CustomDialog mDialog;

    public static ArchiveFragment newInstance() {
        return new ArchiveFragment();
    }

    public ArchiveFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_archive, container, false);
        ButterKnife.bind(this, view);

        mArchivePresenter.takeView(this);
        mArchivePresenter.loadReportFiles();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mArchivePresenter.dropView();
    }

    private void setData(ArrayList<ReportFile> pReportArrayList) {
        setRefreshing(true);
        mArchiveAdapter.setData(pReportArrayList);
        mArchiveAdapter.notifyDataSetChanged();
        if (pReportArrayList.isEmpty()) {
            mNoDataTextView.setVisibility(View.VISIBLE);
        }
    }

    private void setRefreshing(boolean pVisibility) {
        mRecyclerView.setVisibility(pVisibility ? View.VISIBLE : View.GONE);
        mProgressBar.setVisibility(!pVisibility ? View.VISIBLE : View.GONE);
    }

    @Override
    public void initViews() {
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.addItemDecoration(mInsetDecoration);
        mRecyclerView.setAdapter(mArchiveAdapter);
        mArchiveAdapter.setAdapterClickListener(this);
    }

    @UiThread
    @Override
    public void initData(ArrayList<ReportFile> pReportFileArrayList) {
        Objects.requireNonNull(getActivity()).runOnUiThread(() ->
                this.setData(pReportFileArrayList));
    }

    @Override
    public void showDownloadSuccess() {
        setRefreshing(true);
        ToastUtils.showCenter(getContext(), getString(R.string.text_downloaded_successfully));
    }

    @Override
    public void showShareFile(File pFile) {
        Uri uri = Uri.fromFile(pFile);
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        shareIntent.setDataAndType(uri, getContext().getContentResolver().getType(uri));
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
    }

    @Override
    public void showOpenFile(File pFile) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(pFile), "application/excel");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

    @Override
    public void onDeleteClick(ReportFile pReportFile) {
        setRefreshing(false);
        mArchivePresenter.removeStorageFile(pReportFile);
    }

    @Override
    public void onShareClick(ReportFile pReportFile) {

    }

    @Override
    public void onOpenClick(ReportFile pReportFile) {

    }

    @Override
    public void onDownloadClick(ReportFile pReportFile) {
        setRefreshing(false);
        mArchivePresenter.downloadStorageFile(pReportFile);
    }
}
