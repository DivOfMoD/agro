package com.agro.quality_control.ui.activities.auth;

import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.agro.quality_control.di.scopes.AuthScope;

import javax.inject.Inject;

@AuthScope
public class AuthPresenter implements AuthContract.AuthPresenter, AuthContract.AuthModel.OnAuthStateListener {

    @Nullable
    private AuthContract.AuthView mAuthView;
    private AuthModel mAuthModel;

    @Inject
    AuthPresenter(AuthModel pAuthModel) {
        mAuthModel = pAuthModel;
    }

    @Override
    public void signIn(String pEmail, String pPassword) {

        if (!isEmailValid(pEmail) && mAuthView != null) {
            mAuthView.showAuthFailed(AuthContract.AuthEnum.EMAIL, null);
            return;
        }
        if (!isPasswordValid(pPassword) && mAuthView != null) {
            mAuthView.showAuthFailed(AuthContract.AuthEnum.PASSWORD, null);
            return;
        }

        mAuthModel.signInWithEmailAndPassword(pEmail, pPassword);
    }

    private boolean isEmailValid(String pEmail) {
        return !pEmail.isEmpty() && isEmailAddress(pEmail);
    }

    private boolean isPasswordValid(String pPassword) {
        return !pPassword.isEmpty();
    }

    private static boolean isEmailAddress(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    public void takeView(AuthContract.AuthView view) {
        mAuthView = view;
        mAuthModel.addAuthStateListener(this);
    }

    @Override
    public void dropView() {
        mAuthView = null;
        mAuthModel.removeAuthStateListener();
    }

    @Override
    public void onSuccess() {
        if (mAuthView != null) {
            mAuthView.navigateMainScreen();
        }
    }

    @Override
    public void onSuccessAdmin() {
        if (mAuthView != null) {
            mAuthView.navigateAdminScreen();
        }
    }

    @Override
    public void onFailed(Exception pE) {
        if (mAuthView != null)
            mAuthView.showAuthFailed(AuthContract.AuthEnum.FIREBASE, pE);
    }
}
