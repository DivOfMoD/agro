package com.agro.quality_control.ui.fragments.createreport;

import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.agro.quality_control.data.db.AgroDatabase;
import com.agro.quality_control.data.models.LocalDropdown;
import com.agro.quality_control.data.models.Report;

import java.util.ArrayList;

import javax.inject.Inject;

public class CreateReportPresenter implements CreateReportContract.CreateReportPresenter, CreateReportContract.CreateReportPresenter.CreateReportPresenterListener {

    @Nullable
    private CreateReportContract.CreateReportView mCreateReportView;
    private CreateReportModel mCreateReportModel;

    @Inject
    public CreateReportPresenter(CreateReportModel pCreateReportModel) {
        mCreateReportModel = pCreateReportModel;
    }

    @Override
    public void takeView(CreateReportContract.CreateReportView view) {
        mCreateReportView = view;
        mCreateReportModel.addRequestListener(this);
    }

    @Override
    public void dropView() {
        mCreateReportView = null;
        mCreateReportModel.removeRequestListener();
    }

    @Override
    public void initViews() {
        if (mCreateReportView != null) {
            mCreateReportView.initViews();
        }
    }

    @Override
    public void getData() {
        if (mCreateReportModel != null) {
            mCreateReportModel.getDropdownLocalData();
            mCreateReportModel.getDropdownServerData();
        }
    }

    @Override
    public void createReport(Report pReport) {
        ArrayList<CreateReportContract.CreateReportEnum> enumCoreList = getReportCoreEnum(pReport);
        if (!enumCoreList.isEmpty() && mCreateReportView != null) {
            mCreateReportView.showSaveReportCore(enumCoreList);
        } else {
            ArrayList<CreateReportContract.CreateReportEnum> enumDraftList = getReportDraftEnum(pReport);
            if (!enumDraftList.isEmpty() && mCreateReportView != null) {
                mCreateReportView.showSaveReportWithDraft(enumCoreList);
            } else {
                pReport.setSyncState(AgroDatabase.SYNC_READY);
                mCreateReportModel.createReport(pReport);
            }
        }
    }

    @Override
    public void updateReport(Report pReport) {
        ArrayList<CreateReportContract.CreateReportEnum> enumCoreList = getReportCoreEnum(pReport);
        if (!enumCoreList.isEmpty() && mCreateReportView != null) {
            mCreateReportView.showSaveReportCore(enumCoreList);
        } else {
            ArrayList<CreateReportContract.CreateReportEnum> enumDraftList = getReportDraftEnum(pReport);
            if (!enumDraftList.isEmpty() && mCreateReportView != null) {
                mCreateReportView.showSaveReportWithDraft(enumCoreList);
            } else {
                pReport.setSyncState(AgroDatabase.SYNC_READY);
                mCreateReportModel.updateReport(pReport);
            }
        }
    }

    @Override
    public void createDraftReport(Report pReport) {
        pReport.setSyncState(AgroDatabase.SYNC_DRAFT);
        mCreateReportModel.createReport(pReport);
    }

    @Override
    public void updateDraftReport(Report pReport) {
        pReport.setSyncState(AgroDatabase.SYNC_DRAFT);
        mCreateReportModel.updateReport(pReport);
    }

    @Override
    public void getUnloading2Data(int pPosition) {
        mCreateReportModel.getUnloading2Data(pPosition);
    }

    private ArrayList<CreateReportContract.CreateReportEnum> getReportCoreEnum(Report pReport) {
        ArrayList<CreateReportContract.CreateReportEnum> enumList = new ArrayList<>();
        if (mCreateReportView != null) {
            if (TextUtils.isEmpty(pReport.getBoxNumber())) {
                enumList.add(CreateReportContract.CreateReportEnum.BOX_NUMBER);
            }
            if (pReport.getCulture() != null && pReport.getCulture().equals("не указано")) {
                enumList.add(CreateReportContract.CreateReportEnum.CULTURE);
            }
            if (TextUtils.isEmpty(pReport.getDocNumber())) {
                enumList.add(CreateReportContract.CreateReportEnum.DOC_NUMBER);
            }
            if (pReport.getSupplier() != null && pReport.getSupplier().equals("не указано")) {
                enumList.add(CreateReportContract.CreateReportEnum.SUPPLIER);
            }
        }
        return enumList;
    }

    private ArrayList<CreateReportContract.CreateReportEnum> getReportDraftEnum(Report pReport) {
        ArrayList<CreateReportContract.CreateReportEnum> enumList = new ArrayList<>();
        if (mCreateReportView != null) {
            if (pReport.getCulture() != null && pReport.getCulture().equals("1") && TextUtils.isEmpty(pReport.getGluten())) {
                enumList.add(CreateReportContract.CreateReportEnum.CULTURE_GLUTEN);
            }//TODO CHECK THIS IF ПШЕНИЦА, maybe simplify
//            if (pReport.getCulture() != null && pReport.getCulture().equals("1") && TextUtils.isEmpty(pReport.getDrop())) {
//                enumList.add(CreateReportContract.CreateReportEnum.CULTURE_DROP);
//            }
//            if (pReport.getCulture() != null && (pReport.getCulture().equals("5") || pReport.getCulture().equals("3")) && TextUtils.isEmpty(pReport.getDrop())) {
//                enumList.add(CreateReportContract.CreateReportEnum.CULTURE_DROP);
//            }
            if (TextUtils.isEmpty(pReport.getCultureYear())) {
                enumList.add(CreateReportContract.CreateReportEnum.CULTURE_YEAR);
            }
            if (TextUtils.isEmpty(pReport.getWeight())) {
                enumList.add(CreateReportContract.CreateReportEnum.WEIGHT);
            }
            if (TextUtils.isEmpty(pReport.getAutoNumber())) {
                enumList.add(CreateReportContract.CreateReportEnum.AUTO_NUMBER);
            }
            if (TextUtils.isEmpty(pReport.getHumidity())) {
                enumList.add(CreateReportContract.CreateReportEnum.HUMIDITY);
            }
            if (pReport.getUnloading() != null && pReport.getUnloading().equals("не указано")) {
                enumList.add(CreateReportContract.CreateReportEnum.UNLOADING);
            }
            if ((TextUtils.isEmpty(pReport.getUnloading2()) || pReport.getUnloading2().equals("не указано"))) {
                enumList.add(CreateReportContract.CreateReportEnum.UNLOADING2);
            }
        }
        return enumList;
    }

    @Override
    public void onReportCreated() {
        if (mCreateReportView != null) {
            mCreateReportView.showCreateReportSucceed();
        }
    }

    @Override
    public void onReportUpdated() {
        if (mCreateReportView != null) {
            mCreateReportView.showUpdateReportSucceed();
        }
    }

    @Override
    public void onFailed(String pE) {
        if (mCreateReportView != null) {
            mCreateReportView.showCreateReportFailed(pE);
        }
    }

    @Override
    public void onAPIDataLoaded(LocalDropdown pCreateReportModel) {
        if (mCreateReportView != null) {
            mCreateReportView.initData(pCreateReportModel);
        }
    }

    @Override
    public void onAPIDataLoaded(ArrayList<String> pSupplierArrayList) {
        if (mCreateReportView != null) {
            mCreateReportView.initSupplierData(pSupplierArrayList);
        }
    }

    @Override
    public void onUnloading2DataLoaded(ArrayList<String> pUnloading2ArrayList) {
        if (mCreateReportView != null) {
            mCreateReportView.initUploading2Data(pUnloading2ArrayList);
        }
    }
}
