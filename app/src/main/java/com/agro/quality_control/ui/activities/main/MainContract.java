package com.agro.quality_control.ui.activities.main;

import com.agro.quality_control.data.models.Report;
import com.agro.quality_control.ui.base.BasePresenter;

public interface MainContract {

    interface MainView {
        void initViews();

        void navigateAuthPage();

        void showSignOutError(String pE);
    }

    interface MainPresenter extends BasePresenter<MainView> {
        void signOut();
    }

    interface MainModel {
        void signOut();

        interface OnSignOutStateListener {
            void onSuccess();

            void onFailed(Exception pE);
        }

        void removeSignOutStateListener();

        void addSignOutStateListener(OnSignOutStateListener pOnSignOutStateListener);

    }

    interface FragmentListener {
        void onReportClick(Report pReport);
    }

}
