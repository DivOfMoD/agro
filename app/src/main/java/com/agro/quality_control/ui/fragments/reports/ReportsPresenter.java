package com.agro.quality_control.ui.fragments.reports;

import android.support.annotation.Nullable;

import com.agro.quality_control.data.models.Report;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ReportsPresenter implements ReportsContract.ReportsPresenter, ReportsContract.ReportsPresenter.ReportsPresenterListener {

    @Nullable
    private ReportsContract.ReportsView mReportsView;
    private ReportsModel mReportsModel;

    @Inject
     ReportsPresenter(ReportsModel pReportsModel) {
        mReportsModel = pReportsModel;
    }

    @Override
    public void takeView(ReportsContract.ReportsView view) {
        mReportsView = view;
        mReportsView.initViews();
        mReportsModel.addRequestListener(this);
    }

    @Override
    public void dropView() {
        mReportsView = null;
        mReportsModel.removeRequestListener();
    }

    @Override
    public void formatReport(List<Report> pSelectedItems) {
        mReportsModel.uploadReport(pSelectedItems);
    }

    @Override
    public void loadReports(String pFilter) {
        mReportsModel.getReports(pFilter);
    }

    @Override
    public void removeReports(List<Report> pSelectedItems) {
        mReportsModel.deleteReports(pSelectedItems);
    }

    @Override
    public void onReportsLoaded(ArrayList<Report> pReportArrayList) {
        if (mReportsView != null) {
            mReportsView.initData(pReportArrayList);
        }
    }

    /**
     * NO NEED TO HANDLE DELETE IN ADAPTER:
     * JUST RENEW THE COLLECTION IF > 0 DELETED ITEMS
     */
    @Override
    public void onReportsDeleted() {
        this.loadReports("");
    }

    @Override
    public void onArchiveSent() {
        this.loadReports("");
    }

    @Override
    public void onFailed() {
        if (mReportsView != null) {
            mReportsView.showError();
        }
    }

}
