package com.agro.quality_control.ui.fragments.reports;

import android.os.Environment;
import android.util.Log;

import com.agro.quality_control.data.ThreadExecutors;
import com.agro.quality_control.data.api.DataType;
import com.agro.quality_control.data.api.FirebaseRepository;
import com.agro.quality_control.data.api.OnRequestListener;
import com.agro.quality_control.data.db.AgroDatabase;
import com.agro.quality_control.data.models.Report;
import com.agro.quality_control.utils.ConstUtils;
import com.agro.quality_control.utils.ValueUtils;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ReportsModel implements ReportsContract.ReportsModel {

    private FirebaseRepository mFirebaseRepository;
    private AgroDatabase mAgroDatabase;
    private ThreadExecutors mThreadExecutors;
    private ReportsContract.ReportsPresenter.ReportsPresenterListener mReportsPresenterListener;

    @Inject
    ReportsModel(FirebaseRepository pFirebaseRepository, AgroDatabase pAgroDatabase, ThreadExecutors pThreadExecutors) {
        mFirebaseRepository = pFirebaseRepository;
        mAgroDatabase = pAgroDatabase;
        mThreadExecutors = pThreadExecutors;
    }

    @Override
    public void removeRequestListener() {
        mReportsPresenterListener = null;
    }

    @Override
    public void addRequestListener(ReportsContract.ReportsPresenter.ReportsPresenterListener pListener) {
        mReportsPresenterListener = pListener;
    }

    private File generatePDFFile(List<Report> pSelectedItems) {
        String extstoragedir = Environment.getExternalStorageDirectory().toString();
        File fol = new File(extstoragedir, ConstUtils.ROOT_PATH);
        if (!fol.exists()) {
            fol.mkdir();
        }
        try {
            final File file = new File(fol, "Отчет " + String.valueOf(System.currentTimeMillis() + ConstUtils.PDF));
            file.createNewFile();
            FileOutputStream fOut = new FileOutputStream(file);

            Workbook workbook = new HSSFWorkbook();
            Sheet sheet = workbook.createSheet(file.getName());

            Font headerFont = workbook.createFont();
//            headerFont.setBold(true);
            headerFont.setFontHeightInPoints((short) 14);
            headerFont.setColor(IndexedColors.BLACK.getIndex());

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);

            // Create a Row
            Row headerRow = sheet.createRow(4);

            for (int i = 0; i < ConstUtils.COLUMNS.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(ConstUtils.COLUMNS[i]);
                cell.setCellStyle(headerCellStyle);
            }

            // Create Other rows and cells with contacts data
            int rowNum = 5;

            for (int i = 0; i < pSelectedItems.size(); i++) {
                int columnNum = 0;
                Row row = sheet.createRow(rowNum++);
                row.createCell(columnNum++).setCellValue(i + 1);
                row.createCell(columnNum++).setCellValue(ValueUtils.getFormattedDate(pSelectedItems.get(i).getTimestamp()));
                row.createCell(columnNum++).setCellValue(ValueUtils.getFormattedSyncState(pSelectedItems.get(i).getSyncState()));
                row.createCell(columnNum++).setCellValue(pSelectedItems.get(i).getBoxNumber());
                row.createCell(columnNum++).setCellValue(pSelectedItems.get(i).getCulture());
                row.createCell(columnNum++).setCellValue(pSelectedItems.get(i).getCultureYear());
                row.createCell(columnNum++).setCellValue(pSelectedItems.get(i).getDocNumber());
                row.createCell(columnNum++).setCellValue(pSelectedItems.get(i).getWeight());
                row.createCell(columnNum++).setCellValue(pSelectedItems.get(i).getSupplier());
                row.createCell(columnNum++).setCellValue(pSelectedItems.get(i).getUnloading());
                row.createCell(columnNum++).setCellValue(pSelectedItems.get(i).getAutoNumber());
                row.createCell(columnNum++).setCellValue(pSelectedItems.get(i).getHumidity());
                row.createCell(columnNum++).setCellValue(pSelectedItems.get(i).getGluten());
                row.createCell(columnNum++).setCellValue(pSelectedItems.get(i).getGlutenIdk());
                row.createCell(columnNum++).setCellValue(pSelectedItems.get(i).getNature());
                row.createCell(columnNum++).setCellValue(pSelectedItems.get(i).getDrop());
                row.createCell(columnNum++).setCellValue(pSelectedItems.get(i).isPoison());
                row.createCell(columnNum++).setCellValue(pSelectedItems.get(i).getGarbage());
                row.createCell(columnNum++).setCellValue(pSelectedItems.get(i).getSmell());
                row.createCell(columnNum++).setCellValue(pSelectedItems.get(i).getUnloading2());
//                row.createCell(columnNum++).setCellValue(pSelectedItems.get(i));
//                row.createCell(columnNum++).setCellValue(pSelectedItems.get(i));
            }

//            "Пункт разгрузки (цех)",
//                    "Пункт разгрузки (бочка)",
//                    "Пункт разгрузки (силос)"

            // Resize all columns to fit the content size
//            for (int i = 0; i < ConstUtils.COLUMNS.length; i++) {
//                sheet.autoSizeColumn(i);
//            }

//            PdfDocument document = new PdfDocument();
//            PdfDocument.PageInfo pageInfo = new
//                    PdfDocument.PageInfo.Builder(842, 595, 1).create();
//            PdfDocument.Page page = document.startPage(pageInfo);
//            Canvas canvas = page.getCanvas();
//            Paint paint = new Paint();
//
//            for (int i = 0; i < pSelectedItems.size(); i++) {
//                canvas.drawText(pSelectedItems.toString(), 20, (i + 1) * 20, paint);
//                canvas.drawLine(0, (i + 1) * 20, 842, (i + 1) * 20, paint);
//            }
//
//            document.finishPage(page);
//            document.writeTo(fOut);
//
//            document.close();

            workbook.write(fOut);
            fOut.close();
            return file;
        } catch (Exception e) {
            Log.i("error", e.getLocalizedMessage());
        }
        return null;
    }

    @Override
    public void uploadReport(List<Report> pSelectedItems) {
        File file = generatePDFFile(pSelectedItems);
        try {
            mFirebaseRepository.uploadXLS(file,
                    new OnRequestListener<String>() {
                        @Override
                        public void onSucceed(ArrayList<String> pObject) {

                        }

                        @Override
                        public void onSucceed(String pObject) {

                        }

                        @Override
                        public void onSucceed(DataType pDataType) {
                            mReportsPresenterListener.onArchiveSent();
                            file.delete();
                        }

                        @Override
                        public void onFailed(DataType pDataType, String pMessage) {

                        }
                    });
        } catch (FileNotFoundException pE) {
            pE.printStackTrace();
        }
    }

    @Override
    public void getReports(String pFilter) {
        mThreadExecutors.dbExecutor().execute(() ->
        {
            List<Report> reports;
            if (!pFilter.equals("")) {
                final String filter = "%" + pFilter + "%";
                reports = mAgroDatabase.getReportDao().getFilterReport(filter);
            } else {
                reports = mAgroDatabase.getReportDao().getAllReport();
            }
            if (!reports.isEmpty()) {
                mReportsPresenterListener.onReportsLoaded(new ArrayList<>(reports));
            } else {
                mReportsPresenterListener.onFailed();
            }
        });
    }

    @Override
    public void deleteReports(List<Report> pSelectedItems) {
        mThreadExecutors.dbExecutor().execute(() ->
        {
            int count = mAgroDatabase.getReportDao().deleteReports(pSelectedItems);
            if (count > 0) {
                mReportsPresenterListener.onReportsDeleted();
            } else {
                mReportsPresenterListener.onFailed();
            }
        });
    }
}
