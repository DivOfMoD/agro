package com.agro.quality_control.ui.base;

public interface BasePresenter<T> {

    void takeView(T view);

    void dropView();
}
