package com.agro.quality_control.ui.fragments.archive;

import com.agro.quality_control.data.models.ReportFile;
import com.agro.quality_control.ui.base.BaseModel;
import com.agro.quality_control.ui.base.BasePresenter;

import java.io.File;
import java.util.ArrayList;

public interface ArchiveContract {

    interface ArchiveView {

        void initViews();

        void initData(ArrayList<ReportFile> pReportFileList);

        void showDownloadSuccess();

        void showShareFile(File pFile);

        void showOpenFile(File pFile);
    }

    interface ArchivePresenter extends BasePresenter<ArchiveView> {
        interface ArchivePresenterListener {
            void onFileReportsLoaded(ArrayList<ReportFile> pReportArrayList);

            void onDownloaded();

            void onShare();

            void onOpen();

            void onFailed();
        }

        void loadReportFiles();

        void removeStorageFile(ReportFile pReportFile);

        void downloadStorageFile(ReportFile pReportFile);

        void shareStorageFile(ReportFile pReportFile);

        void openStorageFile(ReportFile pReportFile);

    }

    interface ArchiveModel extends BaseModel<ArchivePresenter.ArchivePresenterListener> {
        void getStorageFiles();

        void deleteStorageFile(ReportFile pReportFile);

        void getStorageFile(ReportFile pReportFile);

        void shareStorageFile(ReportFile pReportFile);

        void openStorageFile(ReportFile pReportFile);
    }
}
