package com.agro.quality_control.ui.activities.main;

import com.google.firebase.auth.FirebaseAuth;

import javax.inject.Inject;

public class MainModel implements MainContract.MainModel {

    private FirebaseAuth mFirebaseAuth;
    private OnSignOutStateListener mOnSignOutStateListener;

    @Inject
     MainModel(FirebaseAuth pFirebaseAuth) {
        mFirebaseAuth = pFirebaseAuth;
    }

    @Override
    public void signOut() {
        try {
            mFirebaseAuth.signOut();
            mOnSignOutStateListener.onSuccess();
        } catch (Exception ex) {
            mOnSignOutStateListener.onFailed(ex);
        }
    }

    @Override
    public void removeSignOutStateListener() {
        mOnSignOutStateListener = null;
    }

    @Override
    public void addSignOutStateListener(OnSignOutStateListener pOnSignOutStateListener) {
        mOnSignOutStateListener = pOnSignOutStateListener;
    }
}
