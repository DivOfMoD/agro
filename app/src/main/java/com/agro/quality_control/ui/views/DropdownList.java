package com.agro.quality_control.ui.views;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;

import com.agro.quality_control.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DropdownList extends FrameLayout implements AdapterView.OnItemSelectedListener {

    public final static int TYPE_DROPDOWN = 0;
    public final static int TYPE_EDITTEXT = 1;

    public interface OnDropdownListItemSelected {
        void onItemSelected(int pPosition);
    }

    private OnDropdownListItemSelected mOnDropdownListItemSelected;

    @BindView(R.id.custom_spinner)
    CustomSpinner mCustomSpinner;

    public DropdownList(Context context) {
        super(context);
        init(context);
    }

    public DropdownList(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public DropdownList(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void init(Context pContext) {
        LayoutInflater.from(pContext).inflate(R.layout.view_dropdown_list, this, true);
        ButterKnife.bind(this);
        mCustomSpinner.setOnItemSelectedListener(this);
    }

    public void setSelectAdapter(Activity activity, ArrayList<String> vars, @Nullable OnDropdownListItemSelected pOnDropdownListItemSelected) {
        mCustomSpinner.setSelectAdapter(activity, vars);
        mOnDropdownListItemSelected = pOnDropdownListItemSelected;
    }

    public void setSelection(String pSelection) {
        mCustomSpinner.setSelection(pSelection);
    }

    public String getValue() {
        return mCustomSpinner.getValue();
    }

    public int getCurrentPosition() {
        return mCustomSpinner.getCurrentPosition();
    }

    /**
     * THIS METHOD IS USED FOR UNLOADING HIDDEN FIELDS ONLY
     *
     * @return UNLOADING2 TYPE
     */
    public int getUnloading2Type() {
        switch (getCurrentPosition()) {
            case 1:
            case 5:
            case 6: {
                return TYPE_DROPDOWN;
            }
            case 2:
            case 3:
            case 4: {
                return TYPE_EDITTEXT;
            }
            default: {
                return TYPE_DROPDOWN;
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> pAdapterView, View pView, int pI, long pL) {
        if (mOnDropdownListItemSelected != null) {
            mOnDropdownListItemSelected.onItemSelected(pI);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> pAdapterView) {

    }
}
