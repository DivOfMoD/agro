package com.agro.quality_control.rx;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Map;

import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;

public abstract class RxFirebaseFirestore {

    @NonNull
    public static Observable<QuerySnapshot> getCollection(@NonNull final FirebaseFirestore pFirebaseFirestore,
                                                          @NonNull String pCollection,
                                                          @Nullable String pField,
                                                          @Nullable Object pValue) {
        final Query query =
                pField != null ?
                        pFirebaseFirestore.collection(pCollection).whereEqualTo(pField, pValue) :
                        pFirebaseFirestore.collection(pCollection);

        return Observable.create(emitter -> query.get()
                .addOnSuccessListener(documentSnapshots -> {
                    if (documentSnapshots.isEmpty()) {
                        emitter.onComplete();
                    } else {
                        emitter.onNext(documentSnapshots);
                    }
                })
                .addOnFailureListener(e -> {
                    if (!emitter.isDisposed())
                        emitter.onError(e);
                }));
    }

    @NonNull
    public static Maybe<QuerySnapshot> getSubCollection(@NonNull final FirebaseFirestore pFirebaseFirestore,
                                                        @NonNull String pCollection,
                                                        @NonNull String pDocumentId,
                                                        @NonNull String pSubCollection) {
        final Query query = pFirebaseFirestore.collection(pCollection).document(pDocumentId).collection(pSubCollection);

        return Maybe.create(emitter -> query.get()
                .addOnSuccessListener(documentSnapshots -> {
                    if (documentSnapshots.isEmpty()) {
                        emitter.onComplete();
                    } else {
                        emitter.onSuccess(documentSnapshots);
                    }
                })
                .addOnFailureListener(e -> {
                    if (!emitter.isDisposed())
                        emitter.onError(e);
                }));
    }

    @NonNull
    public static Single<DocumentSnapshot> getDocument(@NonNull final FirebaseFirestore pFirebaseFirestore,
                                                       @NonNull String pCollection,
                                                       @NonNull String pDocumentId) {
        final DocumentReference documentReference = pFirebaseFirestore.collection(pCollection).document(pDocumentId);
        return Single.create(emitter -> documentReference.get()
                .addOnSuccessListener(emitter::onSuccess)
                .addOnFailureListener(emitter::onError));
    }

    @NonNull
    public static Completable setDocument(@NonNull final FirebaseFirestore pFirebaseFirestore,
                                          @NonNull String pCollection,
                                          @NonNull Object pObject) {
        final DocumentReference documentReference = pFirebaseFirestore.collection(pCollection).document();
        return Completable.create(emitter -> documentReference.set(pObject)
                .addOnCompleteListener(result -> {
                    if (result.isSuccessful()) {
                        emitter.onComplete();
                    } else {
                        emitter.onError(new Throwable("NOT SUCCESSFUL"));
                    }
                })
                .addOnFailureListener(emitter::onError));
    }

    @NonNull
    public static Completable deleteDocument(@NonNull final FirebaseFirestore pFirebaseFirestore,
                                             @NonNull String pCollection,
                                             @NonNull String pDocument) {
        final DocumentReference documentReference = pFirebaseFirestore.collection(pCollection).document(pDocument);
        return Completable.create(emitter -> documentReference.delete()
                .addOnCompleteListener(result -> {
                    if (result.isSuccessful()) {
                        emitter.onComplete();
                    } else {
                        emitter.onError(new Throwable("NOT SUCCESSFUL"));
                    }
                })
                .addOnFailureListener(emitter::onError));
    }


    @NonNull
    public static Completable updateDocument(@NonNull final FirebaseFirestore pFirebaseFirestore,
                                             @NonNull String pCollection,
                                             @NonNull String pDocument,
                                             @NonNull String pField,
                                             @NonNull String pValue) {
        final DocumentReference documentReference = pFirebaseFirestore.collection(pCollection).document(pDocument);
        return Completable.create(emitter -> documentReference.update(pField, pValue)
                .addOnCompleteListener(result -> {
                    if (result.isSuccessful()) {
                        emitter.onComplete();
                    } else {
                        emitter.onError(new Throwable("NOT SUCCESSFUL"));
                    }
                })
                .addOnFailureListener(emitter::onError));
    }

    @NonNull
    public static Completable updateDocument(@NonNull final FirebaseFirestore pFirebaseFirestore,
                                             @NonNull String pCollection,
                                             @NonNull Map<String, Object> pObjectMap) {
        final DocumentReference documentReference = pFirebaseFirestore.collection(pCollection).document();
        return Completable.create(emitter -> documentReference.update(pObjectMap)
                .addOnCompleteListener(result -> {
                    if (result.isSuccessful()) {
                        emitter.onComplete();
                    } else {
                        emitter.onError(new Throwable("NOT SUCCESSFUL"));
                    }
                })
                .addOnFailureListener(emitter::onError));
    }

}
