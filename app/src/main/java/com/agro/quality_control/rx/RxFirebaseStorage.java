package com.agro.quality_control.rx;

import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.InputStream;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.annotations.NonNull;

public class RxFirebaseStorage<T> {

    @NonNull
    public static Flowable<String> uploadFile(@NonNull StorageReference pStorageReference,
                                              @NonNull InputStream pInputStream,
                                              @NonNull String... pChildArgs) {
        for (String child : pChildArgs) {
            pStorageReference = pStorageReference.child(child);
        }
        final StorageReference storageReference = pStorageReference;

        Task<UploadTask.TaskSnapshot> uploadTask = storageReference.putStream(pInputStream);
        return Flowable.create(emitter -> uploadTask
                .continueWithTask(task -> storageReference.getDownloadUrl())
                .addOnFailureListener(emitter::onError)
                .addOnCompleteListener(task -> emitter.onNext(task.getResult().toString())), BackpressureStrategy.LATEST);
    }

    @NonNull
    public static Flowable<String> downloadFile(@NonNull StorageReference pStorageReference,
                                                @NonNull File pFile,
                                                @NonNull String... pChildArgs) {
        for (String child : pChildArgs) {
            pStorageReference = pStorageReference.child(child);
        }
        final StorageReference storageReference = pStorageReference;

        FileDownloadTask uploadTask = storageReference.getFile(pFile);
        return Flowable.create(emitter -> uploadTask
                .addOnFailureListener(emitter::onError)
                .addOnCompleteListener(task -> emitter.onNext(task.getResult().toString())), BackpressureStrategy.LATEST);
    }

    @NonNull
    public static Completable deleteFile(@NonNull StorageReference pStorageReference,
                                         @NonNull String... pChildArgs) {
        for (String child : pChildArgs) {
            pStorageReference = pStorageReference.child(child);
        }
        final StorageReference storageReference = pStorageReference;

        Task<Void> uploadTask = storageReference.delete();
        return Completable.create(emitter -> uploadTask
                .addOnFailureListener(emitter::onError)
                .addOnCompleteListener(task -> emitter.onComplete()));
    }
}
